<?php

namespace App\Models;

use App\Models\Admin\Donortype;
use App\Models\Admin\SponsorCategoryType;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'first_name',
        'last_name',
        'spouse_first_name',
        'spouse_last_name',
        'mobile',
        'country',
        'state',
        'city',
        'zip_code',
        'image_url',
        'payment_methord',
        'total_amount',
        'amount_paid',
        'registration_type_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'individual_registration' => 'array'
    ];

    public function registrationtype()
    {
        return $this->belongsTo(SponsorCategoryType::class, 'registration_type_id', 'id' );
    }

    public function categorytype()
    {
        return $this->belongsTo(Donortype::class, 'sponsorship_category_id', 'id');
    }

  /*   public function categorytype()
    {
        return $this->belongsTo(Donortype::class, 'sponsorship_category_id', 'id');
    } */

}
