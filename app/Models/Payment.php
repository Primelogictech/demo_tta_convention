<?php

namespace App\Models;

use App\Models\Admin\Paymenttype;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'payment_amount',
        'payment_methord',
        'unique_id_for_payment',
        'payment_status',
        'more_info',
    ];

    protected $casts = [
        'more_info' => 'array',
    ];

    public function paymentmethord()
    {
        return $this->belongsTo(Paymenttype::class, 'payment_methord');
    }

}
