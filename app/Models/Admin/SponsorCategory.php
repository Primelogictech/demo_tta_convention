<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SponsorCategory extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_type_id',
        'donor_type_id',
        'amount',
        'benefits',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'benefits' => 'array',

    ];

    public function sponsorcategorytype()
    {
        return $this->belongsTo(SponsorCategoryType::class, 'category_type_id', 'id');
    }

    public function donortype()
    {
        return $this->belongsTo(Donortype::class, 'donor_type_id', 'id');
    }

}
