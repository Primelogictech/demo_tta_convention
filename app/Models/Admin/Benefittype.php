<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Benefittype extends Model
{
    use HasFactory;

    protected $table = "benefit_types";

    protected $fillable = ['name', 'status', 'has_count', 'has_input', 'count'];
    protected $casts = [
        'count' => 'integer',
    ];

    public function donors()
    {
        return $this->belongsToMany(SponsorCategory::class, 'benfit_sponsor_category')->withPivot('count');;
    }
}
