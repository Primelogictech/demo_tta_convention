<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Paymenttype;

class RegistrationContent extends Model
{
    use HasFactory;
    protected $table = 'registration_page_content';

    protected $fillable = [
        'additional_donor_benefits',
        'note',
        'check_payable_to',
        'patment_types',
    ];

    protected $casts = [
        'id' => 'integer',
        'patment_types' => 'array',
    ];

    /*     public function paymenttypes()
    {
        return $this->hasMany(Paymenttype::class,'id', 'patment_types.[0]');
    } */

    public function paymenttype($paymenttype)
    {
        return Paymenttype::where('id', $paymenttype)->first();
    }

}
