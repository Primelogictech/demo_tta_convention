<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LogoStoreRequest;
use App\Http\Requests\LogoUpdateRequest;
use App\Models\Admin\Logo;
use Illuminate\Http\Request;

class LeftLogoController extends Controller
{

    public function __construct() {
        $this->type ='left' ;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type=$this->type;
        $logo = Logo::where('type', $this->type)->first();
        return view('admin.master.logo.index', compact('logo', 'type'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $type=$this->type;
        return view('admin.master.logo.create'  ,compact('type'));
    }

    /**
     * @param \App\Http\Requests\LogoStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(LogoStoreRequest $request)
    {
        
        
        $logo = Logo::create($request->validated());

        $file_name = 'logo' . '_' . $logo->id . '.' . $request->image_url->getClientOriginalExtension();
        UploadFile(config('conventions.logo_upload'), $request->file('image_url'), $file_name);

        Logo::where('id', $logo->id)->update(['image_url' => $file_name]);
        return redirect()
        ->route($this->type.'-logo.index')
        ->with('message-suc', 'Logo Added');

    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Logo $logo
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Logo $logo)
    {
        return view('admin.master.logo.show', compact('logo'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Logo $logo
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,  $id)
    {   
        $logo=Logo::find($id);
        $type = $this->type;
        return view('admin.master.logo.edit', compact('logo','type'));
    }

    /**
     * @param \App\Http\Requests\LogoUpdateRequest $request
     * @param \App\Models\Admin\Logo $logo
     * @return \Illuminate\Http\Response
     */
    public function update(LogoUpdateRequest $request, $logo)
    {   

        $logo=logo::find($logo);
        $file_name = 'logo' . '_' . $logo->id . '.' . $request->image_url->getClientOriginalExtension();
        $request->file('image_url')->storeAs(config('conventions.logo_upload'), $file_name);
        
        Logo::where('id', $logo->id)->update(['image_url' => $file_name]);
        
        $request->session()->flash('logo.id', $logo->id);

        return redirect()->route($this->type.'-logo.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Logo $logo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,  $id)
    {
        $logo= logo::find($id);
        return $logo->delete();
    }

    public function updateStatus(Request $request)
    {
        return Logo::where('id', $request->id)->update(['status' => $request->status]);
    }
}
