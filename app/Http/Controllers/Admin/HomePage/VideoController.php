<?php

namespace App\Http\Controllers\Admin\HomePage;

use App\Http\Controllers\Controller;
use App\Http\Requests\VideoStoreRequest;
use App\Http\Requests\VideoUpdateRequest;
use App\Models\Admin\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $videos = Video::all();

        return view('admin.homePageContentUpdate.video.index', compact('videos'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.homePageContentUpdate.video.create');
    }

    /**
     * @param \App\Http\Requests\VideoStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(VideoStoreRequest $request)
    {


        $videoUrl = $request->video_url;
        parse_str(parse_url($videoUrl, PHP_URL_QUERY), $my_array_of_vars);
        if(! isset($my_array_of_vars['v'])){
            $error = \Illuminate\Validation\ValidationException::withMessages([
                'video_url' => ['InValid YouTube URL'],
            ]);
            throw $error;
        }

        $ytID = $my_array_of_vars['v']; //gets video ID
        $data=array_merge($request->validated(), ['youtube_video_id'  => $ytID]);
        $video = Video::create($data);

        $request->session()->flash('video.id', $video->id);

        return redirect()->route('video.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Video $video
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Video $video)
    {
        return view('admin.homePageContentUpdate.video.show', compact('video'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Video $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Video $video)
    {
        return view('admin.homePageContentUpdate.video.edit', compact('video'));
    }

    /**
     * @param \App\Http\Requests\VideoUpdateRequest $request
     * @param \App\Models\Admin\Video $video
     * @return \Illuminate\Http\Response
     */
    public function update(VideoUpdateRequest $request, Video $video)
    {
        $videoUrl = $request->video_url;
        parse_str(parse_url($videoUrl, PHP_URL_QUERY), $my_array_of_vars);
        if (!isset($my_array_of_vars['v'])) {
            $error = \Illuminate\Validation\ValidationException::withMessages([
                'video_url' => ['InValid YouTube URL'],
            ]);
            throw $error;
        }
        $ytID = $my_array_of_vars['v']; //gets video ID
        $data = array_merge($request->validated(), ['youtube_video_id'  => $ytID]);

        $video->update($data);

        $request->session()->flash('video.id', $video->id);

        return redirect()->route('video.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Video $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Video $video)
    {
        return $video->delete();

    }

    public function updateStatus(Request $request)
    {
        return Video::where('id', $request->id)->update(['status' => $request->status]);
    }
}
