<?php

namespace App\Http\Controllers\Admin\HomePage;

use App\Http\Controllers\Controller;
use App\Models\Admin\Venue;
use Illuminate\Http\Request;

class VenueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $venue=Venue::first();
        return view('admin.homePageContentUpdate.venue.viewVenue', compact('venue')) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.homePageContentUpdate.venue.addVenue') ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'start_date' => ['required', 'date', 'after_or_equal:today'],
            'end_date' => ['required', 'date', 'after_or_equal:start_date'],
            'location' => ['required'],
        ];
        $request->validate($rules);
        $data = [
            'Event_date_time' => $request->start_date ,
            'end_date' => $request->end_date,
            'location' => $request->location,
        ];

        if(Venue::first() === null){
            Venue::create($data);
            return redirect()
            ->route('venue.index')
            ->with('message-suc','Venue Added');

        }else{
            return redirect()
                ->route('venue.index')
                ->with('message-suc', 'Venue already added Click on Edit To change');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\venue  $venue
     * @return \Illuminate\Http\Response
     */
    public function show(venue $venue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\venue  $venue
     * @return \Illuminate\Http\Response
     */
    public function edit(venue $venue)
    {
        return view('admin.homePageContentUpdate.venue.editVenue',compact('venue') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\venue  $venue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, venue $venue)
    {
        $rules = [
            'start_date' => ['required' ,'date', 'after_or_equal:today'],
            'end_date' => ['required' ,'date', 'after_or_equal:start_date'],
            'location' => ['required'],
        ];
        $request->validate($rules);
        $data = [
            'Event_date_time' => $request->start_date,
            'end_date' => $request->end_date ,
            'location' => $request->location,
        ];
        Venue::where('id',$venue->id)->update($data);
        return redirect()
            ->route('venue.index')
            ->with('message-suc', 'Venue updated');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\venue  $venue
     * @return \Illuminate\Http\Response
     */
    public function destroy(venue $venue)
    {
        //
    }
}
