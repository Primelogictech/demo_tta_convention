<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\SponsorCategory;
use App\Models\Admin\SponsorCategoryType;
use Illuminate\Http\Request;
use App\Models\Admin\Paymenttype;
use App\Models\User;
//require  base_path() . '/vendor/paypal/sdk/src/Twilio/autoload.php';

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Carbon\Carbon;
use URL;
use Session;
use Config;
use App\Models\Admin\RegistrationContent;
use App\Models\Payment as ModelPayment;

class PaymentController extends Controller
{
     public function __construct()
    {

        $settings = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(env('PAYPAL_SANDBOX_CLIENT_ID'), env('PAYPAL_SANDBOX_CLIENT_SECRET')));
        /** setup PayPal api context **/
        $this->_api_context->setConfig($settings);
    }

    public function index()
    {


        $payer = new Payer();

        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();
        $userCurrency = 'USD';
        $pay_amount = 1221;
        $item_1->setName('NRIVA Convention test')
        ->setCurrency($userCurrency)
            ->setQuantity(1)
            ->setPrice($pay_amount);

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency($userCurrency)
            ->setTotal($pay_amount);
        $timestamp = Carbon::now()->toDateTimeString();

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setInvoiceNumber($timestamp . '_' . '$donation->id')
            ->setItemList($item_list)
            ->setDescription('Transaction Details');


        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('payment.status'))
        ->setCancelUrl(URL::route('payment.status'));
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {

            if (\Config::get('app.debug')) {
                return redirect()->back()->withError('Connection timeout');
            } else {
                return redirect()->back()->withError('Some error occur, sorry for inconvenient');
            }

            // dd($input);

        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            return redirect()->to($redirect_url);
        }
    }


    public function status(Request $request)
    {
        ///donationform/status?paymentId=PAYID-L2GJHIQ52M352183J769814J&token=EC-8LY65310JP231322J&PayerID=MY6L2NR5JMRE6
        $input = $request->input();
        $paymentId = $request->get('paymentId');
        $token = $request->get('token');
        $payerId = $request->get('PayerID');

        $payment_id = Session::get('paypal_payment_id');
        if (empty($payerId) || empty($token)) {
            $result = ['state' => 'User Cancelled'];
            $updateData = [
                'response_message' => json_encode($result),
                'status' => 0
            ];
            //$donation->fill($updateData)->save();
            return redirect(url('/bookticket/'))->withError('Payment failed');
        }

        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        // dd($result);exit;
        $donation = SponsorCategoryType::find($request->session()->get('sponsor_category'));
        // print_r($donation_id);
         Session::forget('paypal_payment_id');

        //echo('<pre>');print($p_order);exit;
        $ModelPayment=ModelPayment::find(Session::get('payment_id'));
        if ($result->getState() == 'approved' && $ModelPayment) {

            $transactions = $result->getTransactions();
            $transaction = $transactions[0];
            $relatedResources = $transaction->getRelatedResources();
            $relatedResource = $relatedResources[0];
            $order = $relatedResource->getSale();

             $updateData = [
              //  'transaction_id' => $order->getId(),
                'more_info' =>  $result,
                'payment_status' => "paid",
                //'order_date' => date("Y-m-d"),
            ];
            $ModelPayment->fill($updateData)->save();
            $id = \Auth::user()->id;
            $user = User::find($id);

            $SponsorCategory = SponsorCategory::where('id', session::get('sponsor_category'))->first();
            $SponsorCategoryType = SponsorCategoryType::where('id', session::get('category_id'))->first();

            $user->sponsorship_category_id = $SponsorCategory->id;
                if( ($user->amount_paid+ $ModelPayment->payment_amount) == $user->total_amount ){
                    $user->payment_status = 'paid';
                }else{
                    $user->payment_status = 'pending';
                }
            $user->total_amount = $SponsorCategory->amount;
            $user->amount_paid = $user->amount_paid + $ModelPayment->payment_amount;
            $user->registration_type_id = $SponsorCategoryType->id;
            $user->save();

            return redirect(url('myaccount'));
        } else {
            $updateData = [
                'response_message' => $result,
                'status' => 0
            ];

            $donation->fill($updateData)->save();
            return redirect(url('/bookticket/'))->withErrors("Payment failed");
        }

    }

    public function payPendingAmount()
    {
        $RegistrationContent = RegistrationContent::first();
        return view('pendingAmountPay', compact('RegistrationContent'));
    }

    public function storePayPendingAmount(Request $request)
    {
        $Paymenttype = Paymenttype::find($request->payment_type);
        $user = User::where('email', $request->email)->first();
        $paying_amount =$request->pending_amount;

        $user = \Auth::user();
        // call payment methord

        if ($Paymenttype->name == 'paypal') {
            return  $this->PaypalPayment($paying_amount, $user->id, $request, $Paymenttype->id, true);
        } else {
            $this->Dopayment($request,  $user,  $paying_amount);
            $user->payment_status = 'Paid';
            $user->amount_paid = $user->amount_paid + $paying_amount;
            $user->save();
            return redirect('myaccount');
        }

    }

    public function PaypalPayment($amount, $user_id, $request,$payment_type_id,$payingPendingAmount=false)
    {
        $settings = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(env('PAYPAL_SANDBOX_CLIENT_ID'), env('PAYPAL_SANDBOX_CLIENT_SECRET')));
        /** setup PayPal api context **/
        $this->_api_context->setConfig($settings);
        $payer = new Payer();

        if($payingPendingAmount){
            $redrict_url_status= 'pendingpayment.status';
        }else{
            $redrict_url_status='payment.status';
        }

        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();
        $userCurrency = 'USD';
        $pay_amount = $amount;
        $item_1->setName('NRIVA Convention test')
            ->setCurrency($userCurrency)
            ->setQuantity(1)
            ->setPrice($pay_amount);

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency($userCurrency)
            ->setTotal($pay_amount);
        $timestamp = Carbon::now()->toDateTimeString();

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setInvoiceNumber($timestamp )
            ->setItemList($item_list)
            ->setDescription('Transaction Details');


        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route($redrict_url_status))
            ->setCancelUrl(URL::route($redrict_url_status));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_api_context);
            $temp = [
                'user_id' => $user_id,
                'payment_amount' => $pay_amount,
                'payment_methord' => $payment_type_id,
                'unique_id_for_payment' =>  $payment->id,
                'more_info' => $request->more_info,
                'payment_status' => 'pending'
            ];
            $ModelPayment = ModelPayment::create($temp);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {

            if (\Config::get('app.debug')) {
                return redirect()->back()->withError('Connection timeout');
            } else {
                return redirect()->back()->withError('Some error occur, sorry for inconvenient');
            }
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if($request->has('sponsor_category')){
            Session::put('sponsor_category', $request->sponsor_category);
        }
        Session::put('payment_id', $ModelPayment->id);
        if (isset($redirect_url)) {
            return redirect($redirect_url);
        }
    }


    public function Dopayment($request, $user,  $paying_amount)
    {
        $Paymenttype = Paymenttype::find($request->payment_type);

        if ($Paymenttype->name == 'check') {
            $temp = [
                'user_id' => $user->id,
                'payment_amount' => $paying_amount,
                'payment_methord' => $Paymenttype->id,
                'unique_id_for_payment' => $request->cheque_number,
                'more_info' => $request->more_info,
                'payment_status' => 'need to verify'
            ];
            $payment = ModelPayment::create($temp);
        }
        if ($Paymenttype->name == 'zelle') {
            $temp = [
                'user_id' => $user->id,
                'payment_amount' => $paying_amount,
                'payment_methord' => $Paymenttype->id,
                'unique_id_for_payment' => $request->Zelle_Reference_Number,
                'more_info' => $request->more_info,
                'payment_status' => 'need to verify'
            ];
            $payment = ModelPayment::create($temp);
        }

        if ($Paymenttype->name == 'other') {
            $temp = [
                'user_id' => $user->id,
                'payment_amount' => $paying_amount,
                'payment_methord' => $Paymenttype->id,
                'unique_id_for_payment' => $request->Zelle_Reference_Number,
                'more_info' => $request->other_payment,
                'payment_status' => 'need to verify'
            ];
            $payment = ModelPayment::create($temp);
        }
    }

    public function pendingstatus(Request $request)
    {
        ///donationform/status?paymentId=PAYID-L2GJHIQ52M352183J769814J&token=EC-8LY65310JP231322J&PayerID=MY6L2NR5JMRE6
        $input = $request->input();
        $paymentId = $request->get('paymentId');
        $token = $request->get('token');
        $payerId = $request->get('PayerID');

        $payment_id = Session::get('paypal_payment_id');
        if (empty($payerId) || empty($token)) {
            $result = ['state' => 'User Cancelled'];
            $updateData = [
                'response_message' => json_encode($result),
                'status' => 0
            ];
            //$donation->fill($updateData)->save();
            return redirect(url('/bookticket/'))->withError('Payment failed');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        // dd($result);exit;
        $donation = SponsorCategoryType::find($request->session()->get('sponsor_category'));
        // print_r($donation_id);
        Session::forget('paypal_payment_id');
        $ModelPayment = ModelPayment::find(Session::get('payment_id'));
        //echo('<pre>');print($p_order);exit;
        if ($result->getState() == 'approved' && $ModelPayment) {

            $transactions = $result->getTransactions();
            $transaction = $transactions[0];
            $relatedResources = $transaction->getRelatedResources();
            $relatedResource = $relatedResources[0];
            $order = $relatedResource->getSale();

            $updateData = [
                //  'transaction_id' => $order->getId(),
                'more_info' =>  $result,
                'payment_status' => "paid",
                //'order_date' => date("Y-m-d"),
            ];
            $ModelPayment->fill($updateData)->save();

            $id = \Auth::user()->id;
            $user = User::find($id);

            $SponsorCategory = SponsorCategory::where('id', session::get('sponsor_category'))->first();
            $SponsorCategoryType = SponsorCategoryType::where('id', session::get('category_id'))->first();
            if (($user->amount_paid + $ModelPayment->payment_amount) == $user->total_amount) {
                $user->payment_status = 'paid';
            } else {
                $user->payment_status = 'pending';
            }
            $user->total_amount = $SponsorCategory->amount;
            $user->amount_paid = $SponsorCategory->amount;
            $user->registration_type_id = $SponsorCategoryType->id;
            $user->save();

            return redirect(url('myaccount'));
        } else {
            $updateData = [
                'response_message' => $result,
                'status' => 0
            ];

            $donation->fill($updateData)->save();
            return redirect(url('/bookticket/'))->withErrors("Payment failed");
        }

    }



}
