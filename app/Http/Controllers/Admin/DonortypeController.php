<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DonortypeStoreRequest;
use App\Http\Requests\DonortypeUpdateRequest;
use App\Models\Admin\Donortype;
use Illuminate\Http\Request;

class DonortypeController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $donortypes = Donortype::all();
        return view('admin.master.donortype.index', compact('donortypes'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      //  
        return view('admin.master.donortype.create');
    }

    /**
     * @param \App\Http\Requests\DonortypeStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(DonortypeStoreRequest $request)
    {
        $donortype = Donortype::create($request->validated());

        $request->session()->flash('donortype.id', $donortype->id);
        return redirect()->route('donortype.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Donortype $donortype
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Donortype $donortype)
    {
        return view('admin.master.donortype.show', compact('donortype'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Donortype $donortype
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Donortype $donortype)
    {
        return view('admin.master.donortype.edit', compact('donortype'));
    }

    /**
     * @param \App\Http\Requests\DonortypeUpdateRequest $request
     * @param \App\Models\Admin\Donortype $donortype
     * @return \Illuminate\Http\Response
     */
    public function update(DonortypeUpdateRequest $request, Donortype $donortype)
    {
        $donortype->update($request->validated());

        $request->session()->flash('donortype.id', $donortype->id);

        return redirect()->route('donortype.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Donortype $donortype
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Donortype $donortype)
    {
        return   $donortype->delete();

    }

    public function updateStatus(Request $request)
    {
        return Donortype::where('id', $request->id)->update(['status' => $request->status]);
    }
}
