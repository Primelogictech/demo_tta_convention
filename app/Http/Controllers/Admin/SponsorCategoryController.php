<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SponsorCategoryStoreRequest;
use App\Http\Requests\SponsorCategoryUpdateRequest;
use App\Models\Admin\Donortype;
use App\Models\Admin\SponsorCategory;
use App\Models\Admin\SponsorCategoryType;
use Illuminate\Http\Request;
use App\Models\Admin\Benefittype;

class SponsorCategoryController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sponsorCategories = SponsorCategory::with('sponsorcategorytype', 'donortype')->get();

        return view('admin.master.sponsorCategory.index', compact('sponsorCategories'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $benfits = Benefittype::where('status',1)->get();
        $SponsorCategoryTypes=SponsorCategoryType::where('status',1)->get();
        $Donortypes=Donortype::where('status',1)->get();
        return view('admin.master.sponsorCategory.create', compact('SponsorCategoryTypes', 'Donortypes','benfits'));
    }

    /**
     * @param \App\Http\Requests\SponsorCategoryStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SponsorCategoryStoreRequest $request)
    {
        $SponsorCategoryType=SponsorCategoryType::where('name', $request->type_id)->first();

        if($request->type_id == 'Family / Individual'){
             foreach ($request->category as $key => $value) {
                $temp=[
                    'category_type_id'=> $SponsorCategoryType->id,
                    'amount'=> $request->amount[$key],
                    'benefits'=> $request->category[$key],
                ];
                $sponsorCategory = SponsorCategory::create($temp);
            }
        }

        if ($request->type_id == 'Donor') {
            $temp=[
                'category_type_id' => $SponsorCategoryType->id,
                'donor_type_id' =>  $request->donor_type_id,
                'amount' => $request->donor_amount,
                'benefits' => $request->benifits,
            ];

            $sponsorCategory = SponsorCategory::create($temp);
        }

        $request->session()->flash('sponsorCategory.id', $sponsorCategory->id);

        return redirect()->route('sponsor-category.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\SponsorCategory $sponsorCategory
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, SponsorCategory $sponsorCategory)
    {
        return view('admin.master.sponsorCategory.show', compact('sponsorCategory'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\SponsorCategory $sponsorCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, SponsorCategory $sponsorCategory)
    {
        $SponsorCategoryTypes = SponsorCategoryType::where('status', 1)->get();
        $Donortypes = Donortype::where('status', 1)->get();
        return view('admin.master.sponsorCategory.edit', compact('sponsorCategory', 'Donortypes', 'SponsorCategoryTypes'));
    }

    /**
     * @param \App\Http\Requests\SponsorCategoryUpdateRequest $request
     * @param \App\Models\Admin\SponsorCategory $sponsorCategory
     * @return \Illuminate\Http\Response
     */
    public function update(SponsorCategoryUpdateRequest $request, SponsorCategory $sponsorCategory)
    {
        $SponsorCategoryType = SponsorCategoryType::where('name', $request->type_id)->first();
        if ($request->type_id == 'Family / Individual') {
            foreach ($request->category as $key => $value) {
                $temp = [
                    'category_type_id' => $SponsorCategoryType->id,
                    'amount' => $request->amount[$key],
                    'benefits' => [$request->category[$key]],
                ];
                SponsorCategory::where('id', $sponsorCategory->id)->update($temp);
            }
        }

        if ($request->type_id == 'Donor') {
            $temp = [
                'category_type_id' => $SponsorCategoryType->id,
                'donor_type_id' =>  $request->donor_type_id,
                'amount' => $request->donor_amount,
                'benefits' => $request->benifits,
            ];

            $sponsorCategory = SponsorCategory::where('id', $sponsorCategory->id)->update($temp);
        }

       // $sponsorCategory->update($request->validated());

       // $request->session()->flash('sponsorCategory.id', $sponsorCategory->id);

        return redirect()->route('sponsor-category.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\SponsorCategory $sponsorCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, SponsorCategory $sponsorCategory)
    {
        return  $sponsorCategory->delete();

        //redirect()->route('sponsorCategory.index');
    }

    public function updateStatus(Request $request)
    {
        return SponsorCategory::where('id', $request->id)->update(['status' => $request->status]);
    }
}
