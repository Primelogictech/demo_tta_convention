<?php

namespace App\Http\Controllers\Admin\Menu;

use App\Http\Controllers\Controller;
use App\Models\Admin\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::withCount('submenus')->where('parent_id',0)->get();
        return view('admin.ContentMangement.Menu.viewmenu', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ContentMangement.Menu.addmenu');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $rules = [
            'name' => ['required',],
        ];
       
        if($request->has('pageType')){
           
            $rules1=[
                'slug' => ['required'],
            ];

            $rules= array_merge($rules, $rules1);
        }

        $request->validate($rules);

        if (!$request->has('pageType')) {
            $request= $request->only('name');
        }else{
            $request= $request->all();
        }
         Menu::create($request);
  
        return redirect()
            ->route('menu.index')
            ->with('message-suc', 'Menu Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        return view('admin.ContentMangement.Menu.editmenu', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        if (!$request->has('pageType')) {
            $request = [
               'name'=> $request->name,
               'slug'=> null, 
            ];
        } else {
            $request = $request->only('name', 'slug');
        }
        
        Menu::where("id", $menu->id)
            ->update($request);

        return redirect()
            ->route('menu.index')
            ->with('message-suc', 'updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu = Menu::find($menu->id);
        return $menu->delete();
    }

    public function updateStatus(Request $request)
    {
        return Menu::where('id', $request->id)->update(['status' => $request->status]);
    }
}

