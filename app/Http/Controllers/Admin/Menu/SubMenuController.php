<?php

namespace App\Http\Controllers\Admin\Menu;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Menu;

class SubMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $submenus = Menu::with('menu')->where('parent_id','!=',0)->get();
        return view('admin.ContentMangement.SubMenu.viewsubmenu', compact('submenus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menus = Menu::where('parent_id', 0)->where('slug',null)->get();
       return view('admin.ContentMangement.SubMenu.addsubmenu', compact('menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => ['required',],
            'slug' => ['required'],
            'parent_id' => ['required'],
        ];

        $request->validate($rules);
        Menu::create($request->all());

        return redirect()
            ->route('submenu.index')
            ->with('message-suc', 'SubMenu Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $submenu=Menu::find($id);
        $menus = Menu::where('parent_id', 0)->where('slug', null)->get();
        return view('admin.ContentMangement.SubMenu.editsubmenu', compact('submenu', 'menus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Menu::where("id", $id)
            ->update([
                'name'=>$request->name,
                'slug'=>$request->slug,
                'parent_id'=>$request->parent_id,
            ]);

        return redirect()
            ->route('submenu.index')
            ->with('message-suc', 'updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::find($id);
        return $menu->delete();
    }

    public function updateStatus(Request $request)
    {
        return Menu::where('id', $request->id)->update(['status' => $request->status]);
    }
    
    public function GetSubmenuWithMenu($id,$edit=false)
    {   
        if($edit){
            return Menu::where('parent_id',$id)
                ->where('page_content', null)
                //->Where('slug', ,null)
                ->orWhere('id', $edit)
                ->get();
        }
        return Menu::where('parent_id',$id)
        ->where('page_content', null)->get();
    }
}
