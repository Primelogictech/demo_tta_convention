<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Benefittype;
use Illuminate\Http\Request;
use App\Http\Requests\BenefittypeStoreRequest;
use App\Http\Requests\BenefittypeUpdateRequest;

class BenefittypeController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $benefittypes = Benefittype::all();
        return view('admin.master.benefittype.index', compact('benefittypes'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      //
        return view('admin.master.benefittype.create');
    }

    /**
     * @param \App\Http\Requests\benefittypeStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BenefittypeStoreRequest $request)
    {
        $benefittype = Benefittype::create($request->validated());

        $request->session()->flash('benefittype.id', $benefittype->id);
        return redirect()->route('benefittype.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\benefittype $benefittype
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Benefittype $benefittype)
    {
        return view('admin.master.benefittype.show', compact('benefittype'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Benefittype $Benefittype
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Benefittype $benefittype)
    {
        return view('admin.master.benefittype.edit', compact('benefittype'));
    }

    /**
     * @param \App\Http\Requests\BenefittypeUpdateRequest $request
     * @param \App\Models\Admin\Benefittype $Benefittype
     * @return \Illuminate\Http\Response
     */
    public function update(BenefittypeUpdateRequest $request, Benefittype $benefittype)
    {
        $temp=[
            'name'=> $request->name
        ];
        if($request->has('has_input')){
            $temp = array_merge($temp,['has_input'=> 1]);
        }else{
            $temp = array_merge($temp,['has_input'=> 0]);
        }

        if($request->has('has_count')){
            $temp = array_merge($temp,['has_count'=> 1]);
            $temp = array_merge($temp,['count'=> $request->count]);
        }else{
            $temp = array_merge($temp, ['has_count' => 0]);
            $temp = array_merge($temp, ['count' => 0]);
        }
        $benefittype->update($temp);
        $request->session()->flash('benefittype.id', $benefittype->id);

        return redirect()->route('benefittype.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\benefittype $benefittype
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Benefittype $benefittype)
    {
        return   $benefittype->delete();

    }

    public function updateStatus(Request $request)
    {
        return Benefittype::where('id', $request->id)->update(['status' => $request->status]);
    }
}
