<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response;
use App\Models\User;
use Session;


class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        //https://nriva.apttechsol.com/public/api/check-credientials
      //  $response = Http::get(env('NRIVA_URL') . '/api/get-check-credientials?email=' . $request->email);
      $response = Http::post(env('NRIVA_URL') . '/api/check-credientials', [
          'email' =>  $request->email,
          'password' => $request->password,
        ]);

        $body = (string) $response->getBody();
        $res=preg_replace("/\r|\n/", "", $body);
         if($res== 'success'){
            $response = Http::get(env('NRIVA_URL') . '/api/get-details?email=' . $request->email);
            $response=$response->json();
            $user= User::where('email', $response['email'])->first();
                if(!isset($user)){
                    $this->createUser($response);
                    $user = User::where('email', $response['email'])->first();
                    $user->member_id = $response['member_id'];
                    $user->save();
                }
            //Session::put('user', $user);
            Auth::login($user);
            $request->session()->regenerate();
                    if($user->email== env('admin_email')){
                        return redirect('admin/registrations');

                    }else{
                        return redirect()->route('bookticket');
                    }
        }else {
            return $res;
        }



    }

    public function createUser($response)
    {
        $data = [
            'email' => $response['email'],
            'first_name' => $response['first_name'],
            'last_name' => $response['last_name'],
            'spouse_first_name' => '',
            'spouse_last_name' => '',
            'phone_code' => $response['phone_code'],
            'mobile' => $response['mobile'],
            'country' => $response['member']['country'],
            'state' => $response['member']['state'],
            'city' => $response['member']['city'],
            'zip_code' => $response['member']['zipcode'],
        ];
        $user = User::create($data);
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
