<?php

namespace App\Http\Controllers;

use App\Models\Admin\SponsorCategory;
use App\Models\Admin\SponsorCategoryType;
use App\Models\Admin\RegistrationContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


use App\Http\Controllers\Controller;
use App\Models\Admin\Paymenttype;
use App\Models\User;
use App\Models\Payment as ModelPayment;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Carbon\Carbon;
use URL;
use Session;
use Config;

class RegistrationController extends Controller
{

    public function __construct()
    {
        $this->_api_context = "";
    }
    public function ShowRegistrationForm()
    {
        $user=User::find(Auth::user()->id);
        if($user->payment_status== 'paid'){
            return redirect('myaccount');
        }
        $SponsorCategoryTypes= SponsorCategoryType::where('status',1)->get();
        $Individuals= SponsorCategory::where('status',1)->where('donor_type_id', 0)->get();
        $donors= SponsorCategory::with('donortype')->where('status',1)->where('donor_type_id','!=' ,0)->get();
        $RegistrationContent = RegistrationContent::first();
        return view('registration',compact("SponsorCategoryTypes", 'Individuals', 'donors', 'RegistrationContent'));
    }
//
    public function RegesterUserStore(Request $request)
    {
        if($request->has('pay_partial_amount')){
            $paying_amount=$request->paying_amount;
        }else{
            if($request->category == 'Donor'){
                $paying_amount= $request->donation_amount_hidden;
            }else{
                $paying_amount= $request->registration_amount_hidden;
            }
        }

        $Paymenttype = Paymenttype::find($request->payment_type);

        $user = User::where('email', $request->email)->first();

        if (!isset($user)) {
            $user = User::create($request->all());
            if ($request->hasfile('image')) {
                $file_name = 'user' . '_' . $user->id . '.' . $request->image->getClientOriginalExtension();
                $request->file('image')->storeAs(config('conventions.user_upload'), $file_name);
                user::where('id', $user->id)->update(['image_url' => $file_name]);
            }
        }

        if($request->category == 'Donor'){
            $category=SponsorCategoryType::where('name', 'Donor')->first();
            $SponsorCategory  = SponsorCategory::find($request->sponsor_category);
            $user = User::where('email', $request->email)->first();
            // call payment methord

            if($Paymenttype->name== 'paypal'){

                $user= Auth::user();
                $user->total_amount = $SponsorCategory->amount;
                $user->save();
                return  $this->PaypalPayment($paying_amount, $user->id, $request, $Paymenttype->id);
            }else{
                $this->Dopayment($request,  $user,  $paying_amount);

                $user->member_id = 'MEM' . $user->id;
                $user->sponsorship_category_id = $SponsorCategory->id;
                if($request->has('pay_partial_amount')){
                    $user->payment_status = 'partial Paid';
                }else{
                    $user->payment_status = 'Paid';
                }
                $user->total_amount = $SponsorCategory->amount;
                $user->amount_paid = $paying_amount;
                $user->registration_type_id = $category->id;
                $user->save();
                return redirect('myaccount');
            }
        }

        if($request->category== "Family / Individual")
        {
            $category = SponsorCategoryType::where('name', 'Family / Individual')->first();
            $SponsorCategory  = SponsorCategory::find($request->sponsor_category);
            $individual_registration=[];
            $total_amount = 0;
            foreach ($request->count as $key => $value) {
                $Individual = SponsorCategory::where('status', 1)->where('id',$key)->first();

                $a=[
                    $key => $value
                ];
                $total_amount = $Individual->amount * $value + $total_amount;
               // $individual_registration = (object)array_merge($a, $individual_registration);
                $individual_registration = $a+ $individual_registration;
            }
            $user = User::where('email', $request->email)->first();
            // call payment methord

            if ($Paymenttype->name == 'paypal') {

                return  $this->PaypalPayment($paying_amount, $user->id, $request, $Paymenttype->id);
            } else {
                $this->Dopayment($request,  $user,  $paying_amount);

                $user->member_id = 'MEM' . $user->id;
                $user->individual_registration =  $individual_registration;
                $user->payment_status = 'paid';
                $user->total_amount = $total_amount;
                $user->amount_paid = $paying_amount;
                $user->registration_type_id = $category->id;
                $user->save();
                //myaccount.blade
                return redirect('myaccount'); //;
            }
        }
      // return  $this->ShowRegistrationForm();
       // return redirect(RouteServiceProvider::HOME);
    }

    public function PaypalPayment($amount, $user_id, $request,$payment_id)
    {

        $settings = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(env('PAYPAL_SANDBOX_CLIENT_ID'), env('PAYPAL_SANDBOX_CLIENT_SECRET')));
        /** setup PayPal api context **/
        $this->_api_context->setConfig($settings);
        $payer = new Payer();

        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();
        $userCurrency = 'USD';
        $pay_amount = $amount;
        $item_1->setName('NRIVA Convention test')
        ->setCurrency($userCurrency)
            ->setQuantity(1)
            ->setPrice($pay_amount);


        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency($userCurrency)
            ->setTotal($pay_amount);
        $timestamp = Carbon::now()->toDateTimeString();

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setInvoiceNumber($timestamp . '_' . '$donation->id')
            ->setItemList($item_list)
            ->setDescription('Transaction Details');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('payment.status'))
        ->setCancelUrl(URL::route('payment.status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
        ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));


        try {
            $payment->create($this->_api_context);

            $temp = [
                'user_id' => $user_id,
                'payment_amount' => $pay_amount,
                'payment_methord' => $payment_id,
                'unique_id_for_payment' => $payment->id,
                'more_info' => $request->more_info,
                'payment_status' => 'pending'
            ];
            $Modelpayment = ModelPayment::create($temp);

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {

            if (\Config::get('app.debug')) {
                return redirect()->back()->withError('Connection timeout');
            } else {
                return redirect()->back()->withError('Some error occur, sorry for inconvenient');
            }
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        Session::put('payment_id', $Modelpayment->id);
        Session::put('sponsor_category', $request->sponsor_category);
        $SponsorCategoryType=SponsorCategoryType::where('name', $request->category)->first();
        Session::put('category_id', $SponsorCategoryType->id);

        if (isset($redirect_url)) {
            return redirect($redirect_url);
        }

    }

    public function Dopayment($request, $user,  $paying_amount)
    {
        $request->payment_type;
        $Paymenttype=Paymenttype::find($request->payment_type);
        if($Paymenttype->name=='check'){

            $temp=[
                'user_id'=> $user->id,
                'payment_amount'=> $paying_amount,
                'payment_methord'=> $Paymenttype->id,
                'unique_id_for_payment'=>$request->cheque_number,
                'more_info'=> $request->more_info,
                'payment_status' => 'need to verify'
            ];
            $payment = ModelPayment::create($temp);

        }
        if($Paymenttype->name== 'zelle'){
            $more_info = [
                'cheque_date' => $request->cheque_date,
                'more_info' =>  $request->more_info
            ];
            $temp = [
                'user_id' => $user->id,
                'payment_amount' => $paying_amount,
                'payment_methord' => $Paymenttype->id,
                'unique_id_for_payment' => $request->Zelle_Reference_Number,
                'more_info' =>   $more_info,
                'payment_status' => 'need to verify'
            ];
            $payment = ModelPayment::create($temp);
        }

        if ($Paymenttype->name == 'other') {
            $more_info=[
                'transaction_date'=> $request->transaction_date,
                'Payment_made_through'=> $request->Payment_made_through,
            ];
            $temp = [
                'user_id' => $user->id,
                'payment_amount' => $paying_amount,
                'payment_methord' => $Paymenttype->id,
                'unique_id_for_payment' => $request->transaction_id,
                'more_info' => $more_info,
                'payment_status' => 'need to verify'
            ];
            $payment = ModelPayment::create($temp);
        }



    }

    public function RegistrationPageContentedit()
    {
        $RegistrationContent=RegistrationContent::first();
        $paymenttypes = Paymenttype::where('status', 1)->get();
        return view('admin.ContentMangement.Registration.contentupdate', compact('RegistrationContent', 'paymenttypes'));
    }

    public function RegistrationPageContentUpdate(Request $request)
    {
        $RegistrationContent = RegistrationContent::first();
        $RegistrationContent->update($request->all());
        $paymenttypes = Paymenttype::where('status', 1)->get();
        return view('admin.ContentMangement.Registration.contentupdate', compact('RegistrationContent', 'paymenttypes'));
    }

    public function getdetailsfromNriva(Request $request)
    {
     //   $response = Http::get('http://localhost/nriva/public/api/get-details?email='.$request->email);
        $response = Http::get(env('NRIVA_URL').'/api/get-details?email='.$request->email);
        return $response;
    }


    public function showRegistrations()
    {
        $users=User::with('registrationtype')->get();
        return view('admin.registrations.index', compact('users'));

    }

    public function myaccount()
    {
        return view('myaccount');
    }


    public function PaymentHistory()
    {

        $payments=ModelPayment::where('user_id', Auth::user()->id)->with('paymentmethord')->get();
        return view('PaymentHistory',compact('payments'));
    }
}
