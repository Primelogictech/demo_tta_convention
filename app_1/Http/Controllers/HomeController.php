<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Venue;
use App\Models\Admin\Menu;
use App\Models\Admin\Message;
use App\Models\Admin\Banner;
use App\Models\Admin\Donor;
use App\Models\Admin\Donortype;
use App\Models\Admin\Event;
use App\Models\Admin\Invitee;
use App\Models\Admin\LeadershipType;
use App\Models\Admin\Program;
use App\Models\Admin\RolesAndDesignations;
use App\Models\Admin\Video;
use PHPUnit\TextUI\XmlConfiguration\Group;

class HomeController extends Controller
{
    public function index()
    {
        $banners=Banner::where('status',1)->get();
        $venue= Venue::first();
        $messages= Message::with('designation')->where('status',1)->get();
        $programs= Program::where('status',1)->get();
        $events= Event::where('status', 1)->get();
        $donortypes= Donortype::where('status',1)->get();
        $leadershiptypes = LeadershipType::where('status', 1)->get();
        $invitees = Invitee::where('status', 1)->get();
        $videos= Video::where('status',1)->take(3)->get();
        return view('index', compact('banners', 'venue', 'messages', 'programs', 'events', 'donortypes', 'videos', 'leadershiptypes', 'invitees'));
    }

    public function dinamicPage($slug)
    {
        $page_data=Menu::where('slug',$slug)->firstOrfail();

        if($page_data==null){
            return abort(404);
        }

        //compact('banners', 'venue', 'messages')
        $page_content=$page_data->page_content;
        return view('dinamicPage',compact('page_content') );
    }

    public function messageContent($id)
    {
        $message=Message::FindOrFail($id);
        $page_content = $message->message;
        return view('dinamicPage', compact('page_content'));
    }

    public function programDetailsPage($id)
    {
        $program = Program::FindOrFail($id);
        return view('programDetailsPage', compact('program'));
    }

    public function eventSchedule($event_id)
    {
        $event= Event::with(['schedules'=>function ($q)
        {
            $q->groupby('date');
        }])->FindOrFail($event_id);
        return view('eventScheduleDetails', compact('event'));
    }

    public function moreDonerDetails($id)
    {
        $donors  =  RolesAndDesignations::with('member')
            ->where('type', 'donorsType')
            ->where('sub_type_id', $id)
            ->get();
        $Donortype =Donortype::find($id);
        return view('moreDonorDetails', compact('donors', 'Donortype') );
    }

    public function moreLeadershipDetails($id)
    {
        $Leaders  =  RolesAndDesignations::with('member', 'designation')
        ->where('type', 'leadershiptype')
        ->where('sub_type_id', $id)
            ->get();
        $LeadershipTypes = LeadershipType::find($id);
        return view('moreLeaderDetails', compact('Leaders', 'LeadershipTypes'));
    }

    public function moreInviteeDetails($id)
    {
        $Invitees  =  RolesAndDesignations::with('member', 'designation')
        ->where('type', 'inviteestype')
        ->where('sub_type_id', $id)
            ->get();
        $invitee = Invitee::find($id);
        return view('moreInveiteDetails', compact('Leaders', 'invitees'));
    }

    public function showAllVideos()
    {
        $videos =Video::where('status',1)->get();
        return view('showAllVideos', compact( 'videos') );
    }

    public function getDonorsWithTypeId($id)
    {
        return RolesAndDesignations::with('member')
                        ->where('type', 'donorsType')
                        ->where('sub_type_id',$id)
                        ->get();
    }

    public function getLeadershipsWithTypeId($id)
    {
        return RolesAndDesignations::with('member', 'designation')
        ->where('type', 'leadershiptype')
        ->where('sub_type_id', $id)
        ->get();
    }
    public function getinviteeWithTypeId($id)
    {
        if($id=='all'){
            return RolesAndDesignations::with('member', 'designation')
            ->where('type', 'inviteestype')
            ->get();
        }
        return RolesAndDesignations::with('member', 'designation')
        ->where('type', 'inviteestype')
        ->where('sub_type_id', $id)
            ->get();
    }

    public function showEventsOnCalander()
    {
        $venue=Venue::first();

        return view('calander' , compact('venue'));
    }

    public function geteventsInLeftSide(Request $request)
    {
        return  Event::
            whereBetween('from_date', [$request->start, $request->end])
            ->orwhereBetween('to_date', [$request->start, $request->end])
            ->get();
    }




    public function geteventsIndates(Request $request)
    {
        $events = Event::
       // whereDate('from_date', '>', $request->start)
        whereBetween('from_date', [$request->start, $request->end])
        ->orwhereBetween('to_date', [$request->start, $request->end])
                    ->with('schedules')->get();
        $data=[];
        foreach ($events as $key => $value) {
            $temp=[
                'title'=> $value->event_name,
                'start'=> ($value->from_date)->format('Y-m-d'),
                'end'=> ($value->to_date)->format('Y-m-d'),
                'url' =>  url('event-schedule', $value->id)
            ];
            array_push($data, $temp);

            foreach ($value->schedules as $schedule) {
                $temp = [
                    'title' => $schedule->program_name,
                    'start' => $schedule->date ." " . $schedule->from_time,
                    'end' =>  $schedule->date . " " .  $schedule->to_time,
                    'url' =>  url('event-schedule', $value->id)
                ];
                array_push($data, $temp);
            }
        }

        return $data;
    }


}
