<?php

namespace App\Http\Controllers\Admin\Menu;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Menu;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Menu::with('menu')
                    ->where('parent_id','!=',0)
                    ->where('page_content','!=',null)
                    ->get();
        return view('admin.ContentMangement.Page.viewpage', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menus = Menu::where('parent_id', 0)->where('slug',null)->get();
       return view('admin.ContentMangement.Page.addpage', compact('menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'id' => ['required'],
            'parent_id' => ['required'],
            'page_content' => ['required'],
        ];

        $request->validate($rules);
        Menu::where('id', $request->id)->update([
            'page_content' => $request->page_content
        ]);

        return redirect()
            ->route('page.index')
            ->with('message-suc', 'Page Content Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page=Menu::find($id);
        $menus = Menu::where('parent_id', 0)->where('slug', null)->get();
        $submenus = Menu::where('parent_id', $page->parent_id )
                    ->Where('slug', null)
                    ->orWhere('id', $page->id)
                    ->get();
        return view('admin.ContentMangement.Page.editpage', compact('page', 'menus', 'submenus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'id' => ['required'],
            'parent_id' => ['required'],
            'page_content' => ['required'],
        ];
        $request->validate($rules);

        Menu::where('id', $request->id)->update([
            'page_content' => $request->page_content
        ]);

        return redirect()
            ->route('page.index')
            ->with('message-suc', 'updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Menu::where('id', $id)->update([
            'page_content' =>null
        ]);
        return 1;
    }

    public function updateStatus(Request $request)
    {
        return Menu::where('id', $request->id)->update(['status' => $request->status]);
    }
}
