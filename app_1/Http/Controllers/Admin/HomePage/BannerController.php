<?php

namespace App\Http\Controllers\Admin\HomePage;

use App\Http\Controllers\Controller;
use App\Models\Admin\Banner;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners=Banner::all();
        return view('admin.homePageContentUpdate.index', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.homePageContentUpdate.addbanner');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules= [
            'banner' => ['required', 'image',
                'mimes:jpg,bmp,png','max:1024'
                //'dimensions:ratio=5/2'
        ],
        ];
        $request->validate($rules);

        $data=[
            'image_url'=>'null',
        ];

        $banner = Banner::create($data);

        $file_name = 'banner'.'_'. $banner->id . '.' . $request->banner->getClientOriginalExtension();
        UploadFile(config('conventions.banner_upload'), $request->file('banner'), $file_name);

        Banner::where('id', $banner->id)->update(['image_url' => $file_name]);
        return redirect()
        ->route('banner.index')
        ->with('message-suc', $request->name . ' Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /*
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::find($id);
        return view('admin.homePageContentUpdate.editBanner', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'banner' => [
                 'image', 'mimes:jpg,bmp,png', 'max:1024',
                'dimensions:ratio=5/2'
            ],
        ];
        $request->validate($rules);

        $banner=Banner::find($id);
        if($request->hasfile('banner') ){
            $file_name = 'banner' . '_' . $banner->id . '.' . $request->banner->getClientOriginalExtension();
            $request->file('banner')->storeAs(config('conventions.banner_upload'), $file_name);
            Banner::where('id',$banner->id)->update(['image_url'=> $file_name ]);
        }
        return redirect()
        ->route('banner.index')
        ->with('message-suc', 'updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner=Banner::find($id);
         if (file_exists(config('conventions.banner_display') . $banner->image_url)) {
            unlink( config('conventions.banner_display').$banner->image_url);
        }
        $banner->delete();
    }

    public function updateStatus(Request $request)
    {
       return Banner::where('id', $request->id)->update(['status'=> $request->status ]);
    }
}
