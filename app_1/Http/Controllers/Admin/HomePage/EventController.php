<?php

namespace App\Http\Controllers\Admin\HomePage;

use App\Http\Controllers\Controller;
use App\Http\Requests\EventStoreRequest;
use App\Http\Requests\EventUpdateRequest;
use App\Models\Admin\Event;
use App\Models\Admin\Venue;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $events = Event::withCount('schedules')->get();

        return view('admin.homePageContentUpdate.event.index', compact('events'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $venue = Venue::first();
        return view('admin.homePageContentUpdate.event.create', compact('venue'));
    }

    /**
     * @param \App\Http\Requests\EventStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventStoreRequest $request)
    {

        $event = Event::create(array_merge($request->all(), ['image_url' => 'null']));

        $file_name = 'event' . '_' . $event->id . '.' . $request->image->getClientOriginalExtension();
        UploadFile(config('conventions.event_upload'), $request->file('image'), $file_name);

        Event::where('id', $event->id)->update(['image_url' => $file_name]);

        $request->session()->flash('event.id', $event->id);

        return redirect()->route('event.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Event $event
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Event $event)
    {
        return view('admin.homePageContentUpdate.event.show', compact('event'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Event $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Event $event)
    {
        $venue = Venue::first();
        return view('admin.homePageContentUpdate.event.edit', compact('event', 'venue'));
    }

    /**
     * @param \App\Http\Requests\EventUpdateRequest $request
     * @param \App\Models\Admin\Event $event
     * @return \Illuminate\Http\Response
     */
    public function update(EventUpdateRequest $request, Event $event)
    {
        $event->update($request->validated());

        if ($request->hasFile('image')) {

            $file_name = 'event' . '_' . $event->id . '.' . $request->image->getClientOriginalExtension();
            $request->file('image')->storeAs(config('conventions.event_upload'), $file_name);

            Event::where('id', $event->id)->update(['image_url' => $file_name]);
        }

        $request->session()->flash('event.id', $event->id);

        return redirect()->route('event.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Event $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Event $event)
    {
        return $event->delete();
    }

    public function updateStatus(Request $request)
    {
        return Event::where('id', $request->id)->update(['status' => $request->status]);
    }
}
