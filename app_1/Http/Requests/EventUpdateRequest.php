<?php

namespace App\Http\Requests;

use App\Models\Admin\Venue;
use Illuminate\Foundation\Http\FormRequest;

class EventUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $venue = Venue::first();
        return [
            'event_name' => ['required', 'string', 'max:100'],
            'from_date' => [
                'required',
                'after_or_equal:'.($venue->Event_date_time)->format('Y-m-d'),
                'before_or_equal:' . ($venue->end_date)->format('Y-m-d')
            ],
            'to_date' => [
                'required',
                'after_or_equal:' . ($venue->Event_date_time)->format('Y-m-d'),
                'before_or_equal:' . ($venue->end_date)->format('Y-m-d')
        ],
            'image' => [ 'mimes:jpg,bmp,png', 'max:1024', 'dimensions:ratio=2/1']
        ];
    }
}
