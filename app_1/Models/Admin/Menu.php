<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;
    protected $fillable = ['name','slug', 'parent_id', 'page_content'];

    public function submenus()
    {
        return $this->HasMany(Menu::class, 'parent_id');
    }

    public function menu()
    {
        return $this->belongsTo(Menu::class, 'parent_id');
    }
}
