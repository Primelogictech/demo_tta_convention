<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolesAndDesignations extends Model
{
    use HasFactory;

    protected $fillable = [
        'member_id',
        'type',
        'sub_type_id',
        'designation_id',
    ];

    public function member()
    {
        return $this->belongsTo(Member::class)->where("members.status",1);
    }
    public function activeMember()
    {
        return $this->belongsTo(Member::class)->where("members.status",1);
    }

    public function designation()
    {
        return $this->belongsTo(Designation::class);
    }

}
