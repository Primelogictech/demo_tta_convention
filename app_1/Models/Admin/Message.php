<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //use HasFactory;
    protected $fillable = ['message', 'designation_id', 'name', 'image_url', 'status'];

    public function designation()
    {
        return $this->belongsTo(Designation::class);
    }

    public function getshortMessage()
    {
       return substr($this->message, 0, 50);
    }

}
