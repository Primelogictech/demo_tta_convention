<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Admin\Venue;
use App\Models\Admin\Menu;
use App\Models\Admin\Logo;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('layouts.user.header', function ($view) {
            $data['venue'] = Venue::first();
           $data['menu'] = Menu::with('submenus')->where('parent_id', 0)->where('status', 1)->get();
            $data['left_logo'] = Logo::where('type', 'left')->first();
            $data['right_logo'] = Logo::where('type', 'right')->first();
            $view->with('data', $data);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
