<?php

namespace Database\Seeders;

use App\Models\Admin\Designation;
use Illuminate\Database\Seeder;

class MasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i=1; $i < 4; $i++) {
            Designation::create([
                'name' => "d" . $i,
            ]);
            
        }
    }
}
