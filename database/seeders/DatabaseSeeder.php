<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Admin\Member::factory(5)->create();
        \App\Models\Admin\Video::factory(5)->create();
        \App\Models\Admin\Donor::factory(5)->create();
        \App\Models\Admin\Event::factory(5)->create();
        \App\Models\Admin\Schedule::factory(5)->create();
        \App\Models\Admin\Program::factory(5)->create();
        \App\Models\Admin\Invitee::factory(3)->create();
        \App\Models\Admin\LeadershipType::factory(3)->create();
        \App\Models\Admin\SponsorCategoryType::factory(3)->create();
        $this->call(LogoTableSeeder::class);
        $this->call(MasterTableSeeder::class);
        $this->call(HomePageTablesSeeder::class); 
        $this->call(DonortypeTableSeeder::class);  
    }
}
