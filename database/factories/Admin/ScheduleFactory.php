<?php

namespace Database\Factories\Admin;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Admin\Event;
use App\Models\Admin\Schedule;

class ScheduleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Schedule::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'event_id' => Event::factory(),
            'date' => $this->faker->date(),
            'to_time' => $this->faker->Time(),
            'from_time' => $this->faker->Time(),
            'program_name' => $this-> faker->regexify('[A-Za-z0-9]{10}'),
            'room_no' => $this-> faker->regexify('[A-Za-z0-9]{10}'),
         //   'image_url' => $this->faker->regexify('[A-Za-z0-9]{100}'),
        ];
    }
}
