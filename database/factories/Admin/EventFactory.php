<?php

namespace Database\Factories\Admin;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Admin\Event;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'event_name' => $this->faker->regexify('[A-Za-z0-9]{100}'),
            'from_date' => $this->faker->dateTime(),
            'to_date' => $this->faker->dateTime(),
            'image_url' => $this->faker->regexify('[A-Za-z0-9]{100}'),
        ];
    }
}
