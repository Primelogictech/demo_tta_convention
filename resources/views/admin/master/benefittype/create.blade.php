@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Add Benefits Type</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('benefittype.create')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('benefittype.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Benefits Type<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" name="name" placeholder="Benefits Type">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Has Input Field<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="checkbox" class="" value="1" name="has_input">
                        </div>
                    </div>

                 <!--    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Has Count Type<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="checkbox" value="1" class="count-checkbox" name="has_count">
                        </div>
                    </div> -->



                    <div class="form-group row count-field" style="display:none">
                        <label class="col-form-label col-md-4 my-auto">Total count<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="number" class="form-control" name="count" placeholder="count">
                        </div>
                    </div>

                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')
<script>
    $(".count-checkbox").change(function() {
        if (IsCheckBoxChecked(this)) {
            $('.count-field').show()
        } else {
            $('.count-field').hide()
        }
    })
</script>


@endsection


@endsection
