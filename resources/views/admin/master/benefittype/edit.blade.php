@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Edit Benefit Type</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ url()->previous() }}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('benefittype.update', $benefittype->id )}}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">benefit Type Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{$benefittype->name}}" name="name" placeholder="benefittype Name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Has Input Field<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="checkbox" class="" value="1" {{ $benefittype->has_input==1 ? 'checked' : ''  }} name="has_input">
                        </div>
                    </div>
<!--
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Has Count Type<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="checkbox" {{ $benefittype->has_count==1 ? 'checked' : ''  }} value="1" class="count-checkbox" name="has_count">
                        </div>
                    </div> -->



                    <div class="form-group row count-field" style="display:none">
                        <label class="col-form-label col-md-4 my-auto">Total count<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="number" class="form-control" value="{{$benefittype->count}}" name="count" placeholder="count">
                        </div>
                    </div>



                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')

<script>
    $(document).ready(function() {
      /*   if ('{{$benefittype->has_count}}' == 1) {
            $('.count-field').show()
        } else {
            $('.count-field').hide()
        }
    })
 */



    $(".count-checkbox").change(function() {
        if (IsCheckBoxChecked(this)) {
            $('.count-field').show()
        } else {
            $('.count-field').hide()
        }
    })
</script>
@endsection

@endsection
