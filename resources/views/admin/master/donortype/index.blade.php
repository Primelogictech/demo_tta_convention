@extends('layouts.admin.base')
@section('content')


<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">donortype</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('donortype.create')}}" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>

                            <tr>
                                <th>Sl NO.</th>
                                <th>donortype Name</th>
                                <th style="width:300px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($donortypes as $donortype)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$donortype->name}}</td>
                                <td>
                                    <a href="{{route('donortype.edit', $donortype->id)}}" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    <button href="#" data-id={{$donortype->id}} data-url='{{ env("APP_URL") }}/admin/donortype/{{$donortype->id}}' class="btn btn-sm btn-success my-1 mx-1  delete-btn">Delete</button>
                                    <button href="#" data-url="{{ env("APP_URL") }}/admin/donortype-status-update" class="btn btn-sm btn-success text-white my-1 mx-1 status-btn" data-id={{$donortype->id}}>{{ $donortype->status=='1' ?  'Active': 'InActive' }}</button>
                                </td>
                            </tr>
                            @empty
                            <p>No donortypes to Show</p>
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
