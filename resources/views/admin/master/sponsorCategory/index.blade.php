@extends('layouts.admin.base')
@section('content')


<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Sponsor Category Types</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('sponsor-category.create')}}" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>

                            <tr>
                                <th>Sl NO.</th>
                                <th>Sponsor Category Type</th>
                                <th>Sponsor Category Name</th>
                                <th>Amount</th>
                                <th>Benefits</th>
                                <th style="width:300px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($sponsorCategories as $sponsorCategorie)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{ $sponsorCategorie->sponsorcategorytype->name }}</td>
                                <td>{{ $sponsorCategorie->donortype->name ?? $sponsorCategorie->benefits }} </td>
                                <td>{{ $sponsorCategorie->amount }} </td>
                                <td><button class="btn btn-sm btn-success text-white my-1 mx-1">View</button> </td>
                                <td>
                                    <a href="{{route('sponsor-category.edit', $sponsorCategorie->id)}}" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    <button data-id={{$sponsorCategorie->id}} data-url='{{ env("APP_URL") }}/admin/sponsor-category/{{$sponsorCategorie->id}}' class="btn btn-sm btn-success my-1 mx-1  delete-btn">Delete</button>
                                    <button data-url="{{ env("APP_URL") }}/admin/sponsor-category-status-update" class="btn btn-sm btn-success text-white my-1 mx-1 status-btn" data-id={{$sponsorCategorie->id}}>{{ $sponsorCategorie->status=='1' ?  'Active': 'InActive' }}</button>
                                </td>
                            </tr>
                            @empty
                            <p>No sponsor categorys to Show</p>
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
