@extends('layouts.admin.base')
@section('content')

<style>
    .checkbox-input {
        position: relative;
        top: 30px;
        width: 16px;
        height: 16px;
    }
</style>

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Edit Sponsor Category Types</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ route('sponsor-category.index') }}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('sponsor-category.update', $sponsorCategory->id )}}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Registration / Sponsor Category <span class="mandatory">*</span></label>
                        @foreach ($SponsorCategoryTypes as $SponsorCategoryType)
                        <div class="col-md-4 my-auto">
                            <input type="radio" data-name='{{ $SponsorCategoryType->name }}' id="btn_{{ $SponsorCategoryType->id }}" value="{{ $SponsorCategoryType->name }}" class="radio-btn" name="type_id">
                            <span class="pl25 ">{{ $SponsorCategoryType->name }}</span>
                        </div>
                        @endforeach
                    </div>

                    <div id='donor' style="display: none;">
                        <div class="form-group row">
                            <label class="col-form-label col-md-4 my-auto">Select Menu<span class="mandatory">*</span></label>
                            <div class="col-md-8 my-auto">
                                <select class="form-control" id='donor_type_id' name="donor_type_id">
                                    <option selected disabled>-- Select --</option>
                                    @foreach($Donortypes as $Donortype)
                                    <option value="{{$Donortype->id}}">{{$Donortype->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-md-4">Enter Amount<span class="mandatory">*</span></label>
                            <div class="col-12 col-md-8">
                                <input type="text" name="donor_amount" id="donor_amount" value="" class="form-control" placeholder="Enter Amount">
                            </div>

                        </div>
                        <!--     <div class="form-group row mb-2">
                            <label class="col-form-label col-md-4 my-auto">Add Benifits<span class="mandatory">*</span></label>
                            <div class="col-12 col-md-8 my-auto">
                                <div class="donor-template donor-add-div">
                                    <div class="row mb-2 main ">
                                        <div class="col-10 col-md-11 my-auto chaild-of-main">
                                            <input type="text" id="benifits_0" name="benifits[]" value="" class="form-control benifit-value" placeholder="">
                                        </div>
                                        <div class="col-2 col-md-1 my-auto">
                                            <div class="button-div">
                                                <span class="plus_n_minus_icons add-category-donor" data-count="0"><i class="fas fa-plus"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
 -->

                        <div class="form-group row mb-2">
                            <label class="col-form-label col-md-4 my-auto">Benifits<span class="mandatory">*</span></label>
                            <div class="col-12 col-md-8 my-auto">
                                <div class="donor-template donor-add-div">
                                    <div class="row mb-2 main ">
                                        @foreach ($benfitstypes as $benfitstype)
                                        <!--  <div class="col-10 col-md-11 my-auto chaild-of-main">
                                            <input type="checkbox" id="benfit_{{ $benfitstype->id }}" name="benifits[]" value="{{ $benfitstype->id }}" class="" placeholder="">
                                        </div>
                                        <label class="col-form-label col-md-4 my-auto">{{ $benfitstype->name }} {{ $benfitstype->has_input ? ', has input' : ""}} {{ $benfitstype->has_count ? "total-". $benfitstype->count : "" }} <span class="mandatory">*</span></label> -->

                                        <label class="col-form-label col-md-4 my-auto">{{ $benfitstype->name }} {{ $benfitstype->has_input ? ', has input' : ""}} {{ $benfitstype->has_count ? "total-". $benfitstype->count : "" }} <span class="mandatory">*</span></label>
                                        <div class="col-5 col-md-5 my-auto chaild-of-main">
                                            <input type="checkbox" id="benfit_{{ $benfitstype->id }}" name="benifits[]" value="{{ $benfitstype->id }}" class="checkbox-input" placeholder="">
                                            <input type="text" id="count_{{ $benfitstype->id }}" name="count[{{ $benfitstype->id }}]" value="" class="form-control ml-4" placeholder="Enter Count if Applicable">
                                        </div>


                                        @endforeach
                                        <!--   <div class="col-2 col-md-1 my-auto">
                                            <div class="button-div">
                                                <span class="plus_n_minus_icons add-category-donor" data-count="0"><i class="fas fa-plus"></i>
                                                </span>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div id='family-or-individual' style="display: none;">

                        <div class="family-or-individual-template">
                            <div class="form-group row main ">
                                <label class="col-form-label col-md-4 chaild-of-main">Enter Registration Category Name<span class="mandatory">*</span></label>
                                <div class="col-12 col-md-4 my-auto">
                                    <input type="text" id="" name="category[]" value="" class="form-control category-value" placeholder="">
                                </div>
                                <div class="col-10 col-md-3 my-auto">
                                    <input type="number" id="" name="amount[]" value="" class="form-control amount-value" placeholder="Enter Price">
                                </div>
                                <div class="col-2 col-md-1 my-auto">
                                    <div class="button-div">
                                        {{-- <span class="plus_n_minus_icons add-category-name" data-count="0"><i class="fas fa-plus"></i>
                                        </span> --}}
                                    </div>
                                    <div class="col-md-4"></div>

                                </div>
                            </div>
                            <div class="family-or-individual-add-div"></div>
                        </div>
                    </div>

                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')


<script>
    $(document).ready(function() {
        sponsorCategory = JSON.parse('{!!json_encode($sponsorCategory) !!}')
        SponsorCategoryTypes = JSON.parse('{!!json_encode($SponsorCategoryTypes) !!}')
        benfits_from_db = JSON.parse('{!!json_encode($sponsorCategory->benfits) !!}')

        benfits_from_db.forEach(data => {
            $('#benfit_' + data.id).attr('checked', 'checked')
            $('#count_' + data.id).val(data.pivot.count)
        });

        for (let i = 0; i < SponsorCategoryTypes.length; i++) {
            if (SponsorCategoryTypes[i].id == sponsorCategory.category_type_id) {
                $('#btn_' + SponsorCategoryTypes[i].id).trigger("click");

                if (SponsorCategoryTypes[i].name == 'Donor') {
                    $('#donor').show();
                    $('#family-or-individual').hide();
                    $('#donor_type_id').val(sponsorCategory.donor_type_id)
                    $('#donor_amount').val(sponsorCategory.amount)
                    for (let i = 0; i < sponsorCategory.benefits.length; i++) {
                        if (i == 0) {
                            $('#benifits_' + i).val(sponsorCategory.benefits[i])
                        } else {
                            addDonorBenifits($('.add-category-donor'), sponsorCategory.benefits[i])
                            $('#benifits_' + i).val(sponsorCategory.benefits[i])

                        }
                    }
                }

                if (SponsorCategoryTypes[i].name == 'Family / Individual') {
                    $('#donor').hide();
                    $('#family-or-individual').show();
                    $('.category-value').val(sponsorCategory.benefits)
                    $('.amount-value').val(sponsorCategory.amount)
                }
            }
        }


        $('.radio-btn').click(function() {
            if ($(this).prop("checked")) {
                if ($(this).data('name') == 'Donor') {
                    $('#donor').show();
                    $('#family-or-individual').hide();
                }

                if ($(this).data('name') == 'Family / Individual') {
                    $('#donor').hide();
                    $('#family-or-individual').show();
                }
            }
        })

        $('body').on('click', '.add-category-name', function() {
            count = $(this).data('count')
            $(this).attr('data-count', parseInt($(this).attr('data-count')) + 1)
            $(".family-or-individual-template").find('.main').first().clone()
                .find(".chaild-of-main").attr('id', 'delete_individual_' + $(this).attr('data-count')).end()
                .find(".category-value").val('').end()
                .find(".amount-value").val('').end()
                .find(".button-div").empty().end()
                .find(".button-div").append('<span class="plus_n_minus_icons delete-item" id="btn_delete_individual_' + $(this).attr('data-count') + '" ><i class="fas fa-minus"></i>').end()
                .appendTo($('.family-or-individual-add-div'));
        })

        $('body').on('click', '.add-category-donor', function() {
            addDonorBenifits(this)

        })

        /*   function addDonorBenifits(thisVal) {
              count = $(thisVal).data('count')
              $(thisVal).attr('data-count', parseInt($(thisVal).attr('data-count')) + 1)
              $(".donor-template").find('.main').first().clone()
                  .find(".chaild-of-main").attr('id', 'delete_donor_' + $(thisVal).attr('data-count')).end()
                  .find(".benifit-value").attr('id', 'benifits_' + $(thisVal).attr('data-count')).end()
                  .find(".benifit-value").val('').end()
                  .find(".button-div").empty().end()
                  .find(".button-div").append('<span class="plus_n_minus_icons delete-item" id="btn_delete_donor_' + $(thisVal).attr('data-count') + '" ><i class="fas fa-minus"></i>').end()
                  .appendTo($('.donor-add-div'));
          } */

        $(document).on("click", ".delete-item", function() {
            id = $(this).attr('id').split('_')
            $('#delete_' + id[2] + '_' + id[3]).parent().remove();
        })

    })
</script>

@endsection

@endsection
