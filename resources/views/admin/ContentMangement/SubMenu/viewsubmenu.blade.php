@extends('layouts.admin.base')
@section('content')


<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Menu Items Managment</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('submenu.create')}}" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Menu Item Name</th>
                                <th>Menu Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($submenus as $submenu )
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$submenu->menu->name}}</td>
                                <td>{{$submenu->name}}</td>
                                <td>
                                    <a href="{{route('submenu.edit', $submenu->id)}}" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    <a href="#" data-id={{$submenu->id}} data-url='{{ env("APP_URL") }}/admin/submenu/{{$submenu->id}}' class="btn btn-sm btn-success my-1 mx-1  delete-btn">Delete</a>
                                    <a href="#" data-url="{{ env("APP_URL") }}/admin/submenu-status-update" class="btn btn-sm btn-success text-white my-1 mx-1 status-btn" data-id={{$submenu->id}}>{{ $submenu->status=='1' ?  'Active': 'InActive' }}</a>
                                </td>
                            </tr>

                            @empty
                            <p>No menu To Show</p>
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection