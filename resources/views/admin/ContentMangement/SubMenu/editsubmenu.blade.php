@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Edit SubMenu</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ url()->previous() }}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('submenu.update', $submenu->id )}}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Select Menu<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <select class="form-control" name="parent_id">
                                <option  disabled>-- Select --</option>
                                @foreach($menus as $menu)
                                <option {{$menu->id ==$submenu->id? "selected"  : ""}} value="{{$menu->id}}">{{$menu->name}}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Menu Item Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{$submenu->name}}" name="name" placeholder="Menu Item Name">
                        </div>
                    </div>



                    <div class="slug-field form-group row">
                        <label class="col-form-label col-md-4 my-auto ">Slug Name<span class="mandatory">*</span></label>
                        <div class="col-md-8  my-auto">
                            <input type="text" class="form-control" name="slug" value="{{$submenu->slug}}" placeholder="Slug">
                        </div>
                    </div>

                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')
<script>
    $(document).ready(function() {
        
    })
</script>

@endsection

@endsection