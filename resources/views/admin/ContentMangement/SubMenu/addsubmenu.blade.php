@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Add SubMenu Item</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('submenu.index')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('submenu.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Select Menu<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <select class="form-control" name="parent_id">
                                <option selected disabled>-- Select --</option>
                                @foreach($menus as $menu)
                                <option value="{{$menu->id}}">{{$menu->name}}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Menu Item Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" name="name" placeholder="Menu Item Name">
                        </div>
                    </div>


                    <div class="slug-field form-group row">
                        <label class="col-form-label col-md-4 my-auto ">Slug Name<span class="mandatory">*</span></label>
                        <div class="col-md-8  my-auto">
                            <input type="text" class="form-control"  name="slug" placeholder="Slug">
                        </div>
                    </div>



                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>


@section('javascript')
<script>

</script>

@endsection


@endsection