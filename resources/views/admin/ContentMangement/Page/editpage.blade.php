@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Edit page</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ url()->previous() }}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('page.update', $page->id )}}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Select Menu<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <select class="form-control  dropdown-element-edit" data-id={{$page->id}} name="parent_id" data-url='{{env('APP_URL')}}/admin/get-submenu-with-menu'>
                                <option selected disabled>-- Select --</option>
                                @foreach($menus as $menu)
                                <option value="{{$menu->id}}" {{ $page->parent_id==$menu->id ? "selected" : ''}}>{{$menu->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Select Submenu<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <select class="form-control dropdown-target"  name="id">
                                <option selected disabled>-- Select --</option>
                                  @foreach($submenus as $submenu)
                                <option value="{{$submenu->id}}" {{ $page->id==$submenu->id ? "selected" : ''}}>{{$submenu->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Add Page Details<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <textarea name="page_content">{{$page->page_content}}</textarea>
                        </div>
                    </div>



                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')
<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>

<script>
	CKEDITOR.replace( 'page_content' );
</script>

@endsection

@endsection