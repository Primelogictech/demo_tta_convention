@extends('layouts.admin.base')
@section('content')


<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Pages</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('page.create')}}" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <b>Note</b>: If SubMenu Is Active then Page will Also become Active and vice versa
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Menu Name</th>
                                <th>Sub Menu Name</th>
                                <th>Page Url (slug in submenu) </th>
                                <th>Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($pages as $page )
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$page->menu->name}}</td>
                                <td>{{$page->name}}</td>
                                <td>
                                <b>Slug</b>:{{$page->slug}}<br>
                                <b>Full Url</b>:
                                <a target="_blank" href="{{env("APP_URL")}}/{{$page->slug}}" >{{env("APP_URL")}}/{{$page->slug}}</a>  </td>
                                <td>
                                  {{--  <a href="" 
                                    class="btn btn-sm btn-success text-white my-1 mx-1">View</a> --}}

                                    <a href="{{route('page.edit', $page->id)}}" 
                                    class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>

                                    <a href="#" data-id={{$page->id}} 
                                    data-url='{{ env("APP_URL") }}/admin/page/{{$page->id}}' 
                                    class="btn btn-sm btn-success my-1 mx-1  delete-btn">Delete</a>

                                    <a href="#" 
                                    data-url="{{ env("APP_URL") }}/admin/page-status-update" 
                                    class="btn btn-sm btn-success text-white my-1 mx-1 status-btn" 
                                    data-id={{$page->id}}>{{ $page->status=='1' ?  'Active': 'InActive' }}</a>

                                </td>
                            </tr>

                            @empty
                            <p>No Pages To Show</p>
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection