@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Edit Menu</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ url()->previous() }}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('menu.update', $menu->id )}}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}
                   
                     <label class="col-form-label col-md-4 my-auto">Menu Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{$menu->name}}" name="name" placeholder="Menu Name">
                        </div>

                        <label class="col-form-label col-md-4 my-auto">Menu Type<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <div class="form-check">
                                <input class="form-check-input  slug-checkbox" {{$menu->slug!=null ? 'checked' : "" }} type="checkbox" value="yes" name="pageType">
                                <label class="form-check-label" for="defaultCheck1">
                                    Check if Menu Redricts to a page
                                </label>
                            </div>
                        </div>

                              <div class="slug-field" style="display:none">
                        <label class="col-form-label col-md-4 my-auto ">Slug Name<span class="mandatory">*</span></label>
                        <div class=" my-auto">
                            <input type="text" class="form-control" name="slug" value="{{$menu->slug}}" placeholder="Slug">
                        </div>
                       </div> 
                  
                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')
<script>
$(document).ready(function () {
    if('{{$menu->slug}}' != "" ){
        $('.slug-field').show()
    }else{
        $('.slug-field').hide()
    }

$(".slug-checkbox").change(function () {
    if(IsCheckBoxChecked(this)){
        $('.slug-field').show()
    }else{
        $('.slug-field').hide()
    }
}) 
})


</script>

@endsection

@endsection