@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Edit Venue Date & Time</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ url()->previous() }}" data-toggle=" tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('venue.update',$venue->id)}}" method="post">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="form-group row">

                        <label class="col-form-label col-md-4 my-auto">Start Date<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="date" class="form-control" value="{{$venue->Event_date_time->format('Y-m-d')}}" name="start_date">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">End Date<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="date" class="form-control" value="{{ $venue->end_date->format('Y-m-d') }}" name="end_date">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Location<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{ $venue->location }}" name="location" placeholder="Location">
                        </div>
                    </div>
                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection
