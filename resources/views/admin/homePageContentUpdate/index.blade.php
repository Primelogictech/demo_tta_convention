@extends('layouts.admin.base')
@section('content')



<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Upload Banners</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('banner.create')}}" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th >Upload Banners</th>
                                <th style="width: 300px;" >Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($banners as $banner)

                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>
                                    <div>
                                        <img src="{{asset(config('conventions.banner_display').$banner->image_url)}}" alt="Banner" width=200 class="img-fluid">
                                    </div>
                                </td>
                                <td>
                                    <a href="{{route('banner.edit',$banner->id)}}" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    <button href="#" class="btn btn-sm btn-success my-1 mx-1 delete-btn" data-url='{{ env("APP_URL") }}/admin/banner/{{$banner->id}}' data-id={{$banner->id}}>Delete</button>
                                    <button href="#" class="btn btn-sm btn-success text-white my-1 mx-1 status-btn" data-url="{{ env('APP_URL') }}/admin/banner-status-update" data-id={{$banner->id}}>{{ $banner->status=='1' ?  'Active': 'InActive' }}</button>

                                </td>
                            </tr>
                            @empty
                            <p>No Banners to show</p>
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@section('javascript')


@endsection

@endsection
