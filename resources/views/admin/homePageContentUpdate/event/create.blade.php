@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Add Event</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('program.create')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('event.store')}}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Event Name <span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{old('event_name')}}" name="event_name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Event From Date<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="date" class="form-control" min="{{ $venue->Event_date_time->format('Y-m-d') }}" max="{{ $venue->end_date->format('Y-m-d') }}" name="from_date">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Event To Date<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="date" class="form-control" min="{{ $venue->Event_date_time->format('Y-m-d') }}" max="{{ $venue->end_date->format('Y-m-d') }}" value="{{old('to_date')}}" name="to_date">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Description<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <textarea name="description"> {{old('description')}}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Upload Event Image <span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="file" class="form-control" name="image">
                            <b>Image should be in 2:1 ration eg: 370X185</b>
                        </div>
                    </div>

                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')


<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>


<script>
    CKEDITOR.replace('description');
</script>



@endsection


@endsection
