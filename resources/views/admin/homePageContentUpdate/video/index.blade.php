@extends('layouts.admin.base')
@section('content')


<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Add Video</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('video.create')}}" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <th>S. No.</th>
                            <th>Video Name</th>
                            <th style="width: 300px;">Upload Videos</th>
                            <th style="width: 300px;">Action</th>
                        </thead>
                        <tbody>
                            @forelse($videos as $video)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$video->name}}</td>
                                <td><img src="https://img.youtube.com/vi/{{$video->youtube_video_id}}/3.jpg"> </td>
                                <td>
                                    <a href="{{route('video.edit', $video->id)}}" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    <button href="#" data-id={{$video->id}} data-url='{{ env("APP_URL") }}/admin/video/{{$video->id}}' class="btn btn-sm btn-success my-1 mx-1  delete-btn">Delete</button>
                                    <button href="#" data-url="{{env("APP_URL") }}/admin/video-status-update" class="btn btn-sm btn-success text-white my-1 mx-1 status-btn" data-id={{$video->id}}>{{ $video->status=='1' ?  'Active': 'InActive' }}</button>
                                </td>
                            </tr>
                            @empty
                            <p>No donortypes to Show</p>
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
