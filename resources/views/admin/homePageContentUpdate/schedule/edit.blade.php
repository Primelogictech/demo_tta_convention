@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Edit schedule</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ url()->previous() }}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->
<h6 id="event-data"></h6>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('schedule.update', $schedules[0]->id )}}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Select Event<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <select class="form-control  event-deopdown" name="event_id">
                                <option disabled>-- Select --</option>
                                @foreach($events as $event)
                                <option {{$schedules[0]->event_id == $event->id? "selected" : ""}} value="{{$event->id}}">{{$event->event_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <!--  <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Select Day<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <select class="form-control dropdown-element" name="day">
                                <option selected disabled>-- Select --</option>
                                <option>Sunday</option>
                                <option>Monday</option>
                                <option>Tuesday</option>
                                <option>Wednesday</option>
                                <option>Thursday</option>
                                <option>Friday</option>
                                <option>Saturday</option>
                            </select>
                        </div>
                    </div> -->

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Date<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="date" id="data-element" class="form-control" value="{{ \Carbon\Carbon::parse($schedules[0]->date)->isoFormat('Y-MM-DD')}}" name="date" placeholder="date">
                        </div>
                    </div>

                    @foreach($schedules as $schedule)
                    <div class="main">
                        <hr>
                        <button href="#" data-id={{$schedule->id}} data-url='{{ env("APP_URL") }}/admin/schedule/{{$schedule->id}}' class="btn btn-sm btn-success my-1 mx-1  delete-btn">Delete</button>

                        <div class="form-group row">
                            <label class="col-form-label col-md-4 my-auto">Start Time<span class="mandatory">*</span></label>
                            <div class="col-md-8 my-auto">
                                <input type="hidden" value="{{$schedule->id}}" name="schedule_id[]">
                                <input type="time" class="form-control" value="{{$schedule->from_time}}" name="from_time[]" placeholder="Start Time">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-md-4 my-auto">End Time<span class="mandatory">*</span></label>
                            <div class="col-md-8 my-auto">
                                <input type="time" class="form-control" value="{{$schedule->to_time}}" name="to_time[]" placeholder="End Time">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-md-4 my-auto">Program Name<span class="mandatory">*</span></label>
                            <div class="col-md-8 my-auto">
                                <input type="text" class="form-control" value="{{$schedule->program_name}}" name="program_name[]" placeholder="Program Name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-md-4 my-auto">Room number<span class="mandatory">*</span></label>
                            <div class="col-md-8 my-auto">
                                <input type="text" class="form-control" value="{{$schedule->room_no}}" name="room_no[]" placeholder="Room number">
                            </div>
                        </div>

                    </div>
                    @endforeach

                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')

<script>
    $('.delete-btn').click(function(e) {
        e.preventDefault()
    })

    events = {!!json_encode($events) !!}

    $('.event-deopdown').change(function() {
        $('#data-element').val('')
        for (let i = 0; i < events.length; i++) {
            if (events[i].id == $(this).val()) {
                console.log(events[i])
                $('#data-element').attr('min', events[i].from_date)
                $('#data-element').attr('max', events[i].to_date)
                $('#event-data').text('Event starts on ' + events[i].from_date + ' end ends on ' + events[i].to_date + " date should be in between this dates")
            }
        }
    })
</script>


@endsection

@endsection
