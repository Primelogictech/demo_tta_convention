@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Add Schedule</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('schedule.create')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>
<h6 id="event-data"><h6>
<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('schedule.store')}}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Select Event<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <select class="form-control dropdown-element event-deopdown" name="event_id">
                                <option selected disabled>-- Select --</option>
                                @foreach($events as $event)
                                <option value="{{$event->id}}">{{$event->event_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <!--  <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Select Day<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <select class="form-control dropdown-element" name="day">
                                <option selected disabled>-- Select --</option>
                                <option>Sunday</option>
                                <option>Monday</option>
                                <option>Tuesday</option>
                                <option>Wednesday</option>
                                <option>Thursday</option>
                                <option>Friday</option>
                                <option>Saturday</option>
                            </select>
                        </div>
                    </div> -->

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Date<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="date" id="data-element" class="form-control" name="date" min="" max="" placeholder="date">
                        </div>
                    </div>

                    <div class="schedule-add-item">
                        <div class="main">
                            <hr>
                            <div class="form-group row">
                                <label class="col-form-label col-md-4 my-auto">Start Time<span class="mandatory">*</span></label>
                                <div class="col-md-8 my-auto">
                                    <input type="time" class="form-control from_time" name="from_time[]" placeholder="Start Time">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-md-4 my-auto">End Time<span class="mandatory">*</span></label>
                                <div class="col-md-8 my-auto">
                                    <input type="time" class="form-control to_time" name="to_time[]" placeholder="End Time">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-md-4 my-auto">Program Name<span class="mandatory">*</span></label>
                                <div class="col-md-8 my-auto">
                                    <input type="text" class="form-control program_name" name="program_name[]" placeholder="Program Name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-md-4 my-auto">Room number<span class="mandatory">*</span></label>
                                <div class="col-md-8 my-auto">
                                    <input type="text" class="form-control room_no" name="room_no[]" placeholder="Room number">
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="new-element"></div>
                    <button class="btn btn-primary add-button">add</button>

                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')


<script>
    $(document).ready(function() {

        events = {!!  json_encode($events)  !!}

        $('.event-deopdown').change(function() {
             $('#data-element').val('')
           for (let i = 0; i < events.length; i++) {
            if(events[i].id==$(this).val()){
                console.log(events[i])
                 $('#data-element').attr('min', events[i].from_date)
                 $('#data-element').attr('max', events[i].to_date)
                 $('#event-data').text('Event starts on '+events[i].from_date+ ' and ends on '+ events[i].to_date + " date should be in between this dates" )
            }
        }
        })

        $('.add-button').click(function(e) {
            e.preventDefault()
            $(".schedule-add-item").find('.main').first().clone()
                .find(".from_time").val('').end()
                .find(".to_time").val('').end()
                .find(".program_name").val('').end()
                .find(".room_no").val('').end()
                .appendTo(".new-element");
        })
    })
</script>

<script>
    $(document).ready(function() {
        event = "{{ app('request')->input('event') ?? 'null'}}"

        if (event != 'null') {
            $(".dropdown-element").val(event).change();
        }
    });
</script>

@endsection


@endsection
