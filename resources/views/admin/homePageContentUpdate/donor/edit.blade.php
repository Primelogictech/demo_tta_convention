@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Edit Messages</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{ url()->previous() }}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('donor.update', $donor->id )}}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Donor Type<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <select class="form-control" name="donortype_id">
                                <option disabled selected>-- Select --</option>
                                @foreach($donorTypes as $donorType)
                                <option {{$donorType->id ==$donor->donortype_id ? 'selected' : "" }} value="{{$donorType->id}}">{{$donorType->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Donor Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{$donor->donor_name}}" name="donor_name" placeholder="Donor Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Description<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <textarea name="description">{{$donor->description}}</textarea>
                        </div>
                    </div>




                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Upload Photo<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="file" class="form-control" name="image">
                        </div>
                        <img src="{{asset(config('conventions.donor_display').$donor->image_url)}}" alt="{{$donor->name}}" width=250px class="img-fluid">
                    </div>
                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>


<script>
    CKEDITOR.replace('description');
</script>

@endsection