@extends('layouts.admin.base')
@section('content')


<style type="text/css">
    .plus-iconn {
        /*border: 1px solid;
    padding: 7px;
    border-radius: 20px;*/
        padding: 3px 8px;
    }
</style>

<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Assign roles to {{$member->name}}</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('member.index')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{url('admin/store-assigne-roles')}}" method="post">
                    @csrf

                    <div class="form-group row">
                        <div class="col-12 pt-2">
                            <div class="row">
                                <div class="col-12 col-lg-3">
                                    <h6 class="mb-3 font-weight-bold">Type</h6>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <h6 class="mb-3 font-weight-bold">Sub-Type</h6>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <h6 class="mb-3 font-weight-bold">Designation</h6>
                                </div>
                            </div>
                                <input type="hidden" value="{{$member->id}}" name="member_id">
                            <div class="row my-2">
                                <div class="col-12 col-sm-6 col-md-3 my-1 my-sm-auto">
                                    <input type="checkbox"
                                    {{isset($rolesAndDesignations->groupBy('type')['leadershiptype']) ?  'checked' : '' }}
                                    class="checbox cursor-pointer" name="Leadership_check"><span class="pl25">Success Leadership</span>
                                </div>

                                <div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
                                    <div>
                                        <select name="Leadership[0][leadershiptype]" id='Leadershi_0_leadershiptype' class="form-control abc">
                                            <option value="">-- Select --</option>
                                            @foreach($leadershiptypes as $leadershiptype)
                                            <option value="{{$leadershiptype->id}}">{{$leadershiptype->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
                                    <div>
                                        <select name="Leadership[0][designation]" id="Leadership_0_designation" class="form-control">
                                            <option value="">-- Select --</option>
                                            @foreach($designations as $designation)
                                            <option value="{{$designation->id}}">{{$designation->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="col-lg-1 my-auto">
                                    <div class="btn btn-primary cursor-pointer plus-iconn add-Leadership" data-count="0" ><i class="fa fa-plus"></i></div>
                                </div>
                            </div>

                            <div class="Leadership-append-div"></div>



                            <div class="row my-2">
                                <div class="col-12 col-sm-6 col-md-3 my-1 my-sm-auto">
                                    <input type="checkbox"
                                    {{isset($rolesAndDesignations->groupBy('type')['inviteestype']) ?  'checked' : '' }}
                                    class="checbox cursor-pointer" name="Invitees_check"><span class="pl25">Convention Invitees</span>
                                </div>
                                <div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
                                    <div class="inviteestype-clone-model">
                                        <select name="invitees[0][inviteestype]" id="invitees_0_inviteestype" class="form-control ">
                                            <option value="">-- Select --</option>
                                            @foreach($invitees as $invitee)
                                            <option value="{{$invitee->id}}">{{$invitee->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
                                    <div>
                                        <select name="invitees[0][designation]" id="invitees_0_designation" class="form-control">
                                            <option value="">-- Select --</option>
                                            @foreach($designations as $designation)
                                            <option value="{{$designation->id}}">{{$designation->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="col-lg-1 my-auto">
                                    <!-- <div class="">
										<i class="fas fa-plus plus-iconn"></i>
									</div> -->
                                    <div class="btn btn-primary cursor-pointer plus-iconn add-invitees" data-count="0" ><i class="fa fa-plus"></i></div>
                                </div>
                            </div>
                            <div class="invitees-append-div"></div>



                            <div class="row my-2">
                                <div class="col-12 col-sm-6 col-md-3 my-1 my-sm-auto">
                                    <input type="checkbox"
                                    {{isset($rolesAndDesignations->groupBy('type')['donorsType']) ?  'checked' : '' }}
                                     class="checbox cursor-pointer" name="donors_check"><span class="pl25">Convention Donors</span>
                                </div>
                                <div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
                                    <div class="">
                                        <select name="donor[0][donorsType]" id="donor_0_donorsType" class="form-control ">
                                            <option value="">-- Select --</option>
                                            @foreach($donortypes as $donortype)
                                            <option value="{{$donortype->id}}">{{$donortype->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
                                    <div>
                                        <select name="donor[0][designation]" id="donor_0_designation" class="form-control">
                                            <option value="">-- Select --</option>
                                            @foreach($designations as $designation)
                                            <option value="{{$designation->id}}">{{$designation->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="col-lg-1 my-auto">
                                    <!-- <div class="">
										<i class="fas fa-plus plus-iconn"></i>
									</div> -->
                                    <div class="btn btn-primary cursor-pointer plus-iconn add-donor" data-count="0" ><i class="fa fa-plus"></i></div>
                                </div>
                            </div>

                            <div class="donors-append-div"></div>

                        </div>
                    </div>

                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- html for cloning  -->
<div class="add-element-leader" style="display:none">
    <div class="row">
        <div class="col-lg-3 my-1 my-sm-auto main">
        </div>
        <div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
            <div class="replace-div">
                <select name="Leadership[][leadershiptype]" class="form-control leadershiptype-dropdown">
                    <option value="">-- Select --</option>
                    @foreach($leadershiptypes as $leadershiptype)
                    <option value="{{$leadershiptype->id}}">{{$leadershiptype->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
            <div>
                <select name="Leadership[][designation]" class="form-control designation-dropdown">
                    <option value="">-- Select --</option>
                    @foreach($designations as $designation)
                    <option value="{{$designation->id}}">{{$designation->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
         <div class="btn btn-danger cursor-pointer plus-iconn delete-item" ><i class="fa fa-trash"></i></div>
    </div>
</div>

<div class="add-element-Invitees" style="display:none">
    <div class="row">
        <div class="col-lg-3 my-1 my-sm-auto main">
        </div>
        <div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
            <div class="replace-div">
                <select name="invitees[][invitees]" class="form-control Invitees-dropdown">
                    <option value="">-- Select --</option>
                    @foreach($invitees as $invitee)
                    <option value="{{$invitee->id}}">{{$invitee->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
            <div>
                <select name="invitees[][designation]" class="form-control designation-dropdown">
                    <option value="">-- Select --</option>
                     @foreach($designations as $designation)
                    <option value="{{$designation->id}}">{{$designation->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
         <div class="btn btn-danger cursor-pointer plus-iconn delete-item" ><i class="fa fa-trash"></i></div>
    </div>
</div>

<div class="add-element-donors" style="display:none">
    <div class="row" >
        <div class="col-lg-3 my-1 my-sm-auto main" >
        </div>
        <div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
            <div class="replace-div">
                <select name="donor[][donorsType]" class="form-control donors-dropdown">
                    <option value="">-- Select --</option>
                    @foreach($donortypes as $donortype)
                    <option value="{{$donortype->id}}">{{$donortype->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-4 my-1 my-sm-auto">
            <div>
                <select name="donor[][designation]" class="form-control designation-dropdown">
                    <option value="">-- Select --</option>
                     @foreach($designations as $designation)
                    <option value="{{$designation->id}}">{{$designation->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
          <div class="btn btn-danger cursor-pointer plus-iconn delete-item" ><i class="fa fa-trash"></i></div>
    </div>
</div>


<!-- end -->
<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready(function() {
       rolesAndDesignations = {!! json_encode($rolesAndDesignations)!!}

        @if(isset($rolesAndDesignations->groupBy('type')['leadershiptype']))
        count=0
            for (let i = 0; i < rolesAndDesignations.length; i++) {
                if(rolesAndDesignations[i].type=='leadershiptype'){
                    if(count==0){
                         $('#Leadershi_0_leadershiptype').val(rolesAndDesignations[i].sub_type_id).change();
                         $('#Leadership_0_designation').val(rolesAndDesignations[i].designation_id).change();
                       //  $('.add-Leadership').attr('data-count',parseInt($('.add-Leadership').attr('data-count'))+1)
                    }else{
                         addLeadershipDiv($('.add-Leadership'))
                         $('#Leadership_'+count+'_leadershiptype').val(rolesAndDesignations[i].sub_type_id).change();
                         $('#Leadership_'+count+'_designation').val(rolesAndDesignations[i].designation_id).change();
                    }
                    count=count+1
                }
            }
        @endif

    @if(isset($rolesAndDesignations->groupBy('type')['inviteestype']))
        count=0
            for (let i = 0; i < rolesAndDesignations.length; i++) {
                if(rolesAndDesignations[i].type=='inviteestype'){
                    if(count==0){
                         $('#invitees_0_inviteestype').val(rolesAndDesignations[i].sub_type_id).change();
                         $('#invitees_0_designation').val(rolesAndDesignations[i].designation_id).change();
                       //  $('.add-Leadership').attr('data-count',parseInt($('.add-Leadership').attr('data-count'))+1)
                    }else{
                          addInvitees($('.add-invitees'))
                         $('#invitees_'+count+'_inviteestype').val(rolesAndDesignations[i].sub_type_id).change();
                         $('#invitees_'+count+'_designation').val(rolesAndDesignations[i].designation_id).change();
                    }
                    count=count+1
                }
            }
    @endif

     @if(isset($rolesAndDesignations->groupBy('type')['donorsType']))
        count=0
            for (let i = 0; i < rolesAndDesignations.length; i++) {
                if(rolesAndDesignations[i].type=='donorsType'){
                    if(count==0){
                         $('#donor_0_donorsType').val(rolesAndDesignations[i].sub_type_id).change();
                         $('#donor_0_designation').val(rolesAndDesignations[i].designation_id).change();
                       //  $('.add-Leadership').attr('data-count',parseInt($('.add-Leadership').attr('data-count'))+1)
                    }else{
                        addDonor($('.add-donor'))
                         $('#donor_'+count+'_donorsType').val(rolesAndDesignations[i].sub_type_id).change();
                         $('#donor_'+count+'_designation').val(rolesAndDesignations[i].designation_id).change();
                    }
                    count=count+1
                }
            }
    @endif

       $(document).on("click", ".delete-item", function() {
           id=$(this).attr('id').split('_')
        $('#delete_'+id[1]+'_'+id[2]).parent().empty()
    })
    });


     $(".add-Leadership").click(function(e) {
          addLeadershipDiv(this)
        });

      $(".add-invitees").click(function(e) {
           addInvitees(this)
        });

       $(".add-donor").click(function(e) {
           addDonor(this)
        });


    function addDonor(thisVal){alert()
             $(thisVal).attr('data-count',parseInt($(thisVal).attr('data-count'))+1)
             $(".add-element-donors").find('.row').first().clone()
            .find(".donors-dropdown").attr('name','donor['+$(thisVal).attr('data-count')+'][donorsType]').end()
            .find(".donors-dropdown").attr('id','donor_'+$(thisVal).attr('data-count')+'_donorsType').end()
            .find(".designation-dropdown").attr('name','donor['+$(thisVal).attr('data-count')+'][designation]').end()
            .find(".designation-dropdown").attr('id','donor_'+$(thisVal).attr('data-count')+'_designation').end()
            .find(".main").attr('id','delete_donor_'+$(thisVal).attr('data-count')).end()
            .find(".delete-item").attr('id','btn_donor_'+$(thisVal).attr('data-count')).end()
           .appendTo($('.donors-append-div'));
    }

    function addInvitees(thisVal){
         $(thisVal).attr('data-count',parseInt($(thisVal).attr('data-count'))+1)
             $(".add-element-Invitees").find('.row').first().clone()
            .find(".Invitees-dropdown").attr('name','invitees['+$(thisVal).attr('data-count')+'][inviteestype]').end()
            .find(".Invitees-dropdown").attr('id','invitees_'+$(thisVal).attr('data-count')+'_inviteestype').end()
            .find(".designation-dropdown").attr('name','invitees['+$(thisVal).attr('data-count')+'][designation]').end()
            .find(".designation-dropdown").attr('id','invitees_'+$(thisVal).attr('data-count')+'_designation').end()
             .find(".main").attr('id','delete_invitees_'+$(thisVal).attr('data-count')).end()
            .find(".delete-item").attr('id','btn_invitees_'+$(thisVal).attr('data-count')).end()
           .appendTo($('.invitees-append-div'));
    }


    function addLeadershipDiv(thisVal){
         $(thisVal).attr('data-count',parseInt($(thisVal).attr('data-count'))+1)
             $(".add-element-leader").find('.row').first().clone()
             .find(".leadershiptype-dropdown").attr('name','Leadership['+$(thisVal).attr('data-count')+'][leadershiptype]').end()
             .find(".leadershiptype-dropdown").attr('id','Leadership_'+$(thisVal).attr('data-count')+'_leadershiptype').end()
             .find(".designation-dropdown").attr('name','Leadership['+$(thisVal).attr('data-count')+'][designation]').end()
             .find(".designation-dropdown").attr('id','Leadership_'+$(thisVal).attr('data-count')+'_designation').end()
              .find(".main").attr('id','delete_leader_'+$(thisVal).attr('data-count')).end()
            .find(".delete-item").attr('id','btn_leader_'+$(thisVal).attr('data-count')).end()
           .appendTo($('.Leadership-append-div'));
    }


</script>



<script>
</script>

@endsection
