@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-5">
    @if($page_content!=null)
    {!! $page_content !!}
    @else
    <h4 class="my-5 text-center text-skyblue">
        Coming Soon ...
    </h4>
    @endif
    
</section>



@section('javascript')

@endsection


@endsection