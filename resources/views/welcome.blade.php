@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-0 my-md-1">
    <div class="row">
        <div class="col-12 py-0 px-1 p-lg-0">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach ($banners as $banner)
                    <div class="carousel-item  {{$loop->first ? 'active' : ''}} ">
                        <img class="d-block w-100" src="{{asset(config('conventions.banner_display').$banner->image_url)}}" alt="First slide" />
                    </div>
                    @endforeach
                    {{-- <div class="carousel-item">
                        <img class="d-block w-100" src="images/banner.jpg" alt="Second slide" />
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="images/banner.jpg" alt="Third slide" />
                    </div>  --}}
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <!-- <span class="carousel-control-prev-icon" aria-hidden="true"></span> -->
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <!-- <span class="carousel-control-next-icon" aria-hidden="true"></span> -->
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<!-- End of Carousel Banners -->

<!-- Location section -->

<section class="container-fluid violet-gradient-bg mt-1 px-4 px-md-5">
    <div class="row">
        <div class="col-12 col-lg-3 text-white border-right border-right-md-0 px-0 px-lg-2 my-auto">
            <a href="{{url('show-events-on-calander')}}" class="text-white text-decoration-none">
                <div class="d-flex my-auto text-center py-4 py-lg-5">
                    <div>
                        <i class="far fa-calendar-alt date-icon"></i>
                    </div>
                    <div class="my-auto">
                        <!-- <span class="fs25 text-white">Jul 1<sup>st</sup> to 3<sup>rd</sup>, 2022<br /></span> -->
                        <span class="fs25 text-white">{{ \Carbon\Carbon::parse($venue->Event_date_time)->isoFormat('MMM Do')}} To {{ \Carbon\Carbon::parse($venue->end_date)->isoFormat('MMM Do')}}, {{ \Carbon\Carbon::parse($venue->end_date)->isoFormat('YYYY')}} </span>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-12 col-lg-6 text-white border-right border-right-md-0 px-0 px-lg-4 my-auto">
            <div class="d-flex text-center py-4 py-lg-5">
                <div class="my-auto">
                    <i class="fas fa-map-marker-alt location-point-icon"></i>
                </div>
                <div class="my-auto">
                    <h4 class="text-yellow text-left text-lg-center fs23">{{$venue->location}}</h4>

                </div>
            </div>
        </div>

        <div class="col-12 col-lg-3 text-white pt-3 pb-4 py-lg-0 my-auto">
            <div class="text-center text-lg-right text-xl-center">
                <a href="registration.php" class="btn register-today-btn">REGISTER TODAY</a>
            </div>
        </div>
    </div>
</section>

<!-- Location section -->

<!-- Crackers -->

<!-- <div class="container-fluid">
    <div class="row">
        <canvas id="canvas"></canvas>
    </div>
</div> -->

<!-- Crackers -->

<!-- Messages -->

<section class="container-fluid mt-1 mb-3 messages-bg px-4 px-md-5" id="messages">
    <div class="row">

        @foreach($messages as $message)
        <div class="col-12 col-sm-6 col-md-6 col-lg-3 py-4 py-lg-5 msg_bg" data-aos="fade-left" data-aos-duration="1000">
            <div>
                <h6 class="president-message pl-3 mb-3">{{$message->designation->name}} Message</h6>
            </div>
            <div class="border-top-violet bg-white px-3 py-4 shadow-sm">
                <div class="borders">
                    <img src="{{asset(config('conventions.message_display').$message->image_url)}}" class="mx-auto d-block rounded-circle" alt="" />
                </div>
                <div class="pt-0 pb-2">
                    <div class="text-center mb-1 fs16 font-weight-bold">{{$message->name}}</div>
                    <div class="text-center text-muted fs14">{{$message->designation->name}}</div>
                </div>
                <hr class="my-0" />
                <p class="pt-3 mb-0 text-center description">
                    {!! substr($message->message, 0, 100) !!}....
                </p>
                <div class="text-center pt-3">
                    <a href="{{url('message_content',$message->id)}}">
                        <img src="images/right-arrow.png" class="img-fluid" alt="" />
                    </a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</section>

<!-- End of Messages -->

<!-- Pillars Of Our Success Leadership -->

<div class="container-fluid">
    <div class="row">
        <div class="col-12 pt-4 pb-md-4 leadership-heading-bottom">
            <div class="main-heading">
                <div>
                    Pillars Of Our Success Leadership
                </div>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid leadership-bg">
    <div class="row pr-3">
        <div class="col-12 col-lg-2 py-0 py-lg-5">
            <div>
                <ul class="list-unstyled leadership-tabs-ul mb-md-0">
                    @foreach ($leadershiptypes as $leadershiptype)
                    <li class="py-4 leadership-nav-item" data-aos="fade-right">
                        <a class="leadership-nav-link text-white text-decoration-none   {{$loop->first ? 'first-leadership-type': '' }}" data-id="{{$leadershiptype->id}}" data-url="{{env('APP_URL')}}/api/get-leadership-with-typeid/{{$leadershiptype->id}}">
                            <span>{{$leadershiptype->name}}</span> <i class="fas fa-long-arrow-alt-right fs22 align-middle leadership-navbar-right-arrow"></i>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-12 col-lg-9 leaders-bg pt20 border-radius-15 ml-2 px-4 px-md-5 leadership-bg-top">
            <div class="row leadership_active_tab leadership-add-div">

                {{-- <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50">
                    <div class="hexagon">
                        <div>
                            <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle mt-40" width="100" alt="" />
                        </div>
                        <div class="text-center my-2">
                            <a href="#" class="leader-name">Mr. Hari Raini</a>
                        </div>
                        <div class="text-center mb10 text-white">President</div>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
    </div>
</section>

<!-- End of Pillars Of Our Success Leadership -->

<!-- Convention Programs -->

<section class="container-fluid my-5 mb-lg-5 mt-lg-125">
    <div class="row">
        <div class="col-12 pb-4">
            <div class="main-heading">
                <div>
                    Convention Programs
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            @foreach($programs as $program)
            <div class="col-12 col-md-4 my-3" data-aos="fade-down">
                <a href="{{url('programs',$program->id)}}">
                    <div>
                        <img style="object-fit:contain;height: 220px" src="{{asset(config('conventions.program_display').$program->image_url)}}" class="img-fluid mx-auto d-block border-radius-10" alt="" />
                    </div>
                </a>
                <div class="pt-3">
                    <h5 class="text-left text-sm-center text-md-left">{{$program->program_name}}</h5>
                </div>
            </div>
            @endforeach



        </div>
    </div>
</section>

<!-- End of Convention Programs -->

<!-- Convention Invitees -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12 pb50">
            <div class="main-heading">
                <div>
                    Convention Invitees
                </div>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid invitees_bg py-5 mb-5">
    <div class="row pr-3">
        <div class="col-12 col-md-3 col-lg-2">
            <div>
                <ul class="list-unstyled invitees-tabs-ul mb-0 pt-0 pt-md-5">
                    <li class="py-4 invitees-nav-item" data-aos="fade-right">
                        <a class="invitees-nav-link text-white text-decoration-none fs18 invitees_active_tab_btn invitees_all_data" data-id="all" data-url="{{env('APP_URL')}}/api/get-invitee-with-typeid/all"><span>All</span> <i class="fas fa-long-arrow-alt-right fs22 align-middle invitees-navbar-right-arrow"></i></a>
                    </li>
                    @foreach ($invitees as $invitee)
                    <li class="py-4 invitees-nav-item" data-aos="fade-right">
                        <a class="invitees-nav-link text-white text-decoration-none fs18" data-id="{{$invitee->id}}" data-url="{{env('APP_URL')}}/api/get-invitee-with-typeid/{{$invitee->id}}"><span>{{$invitee->name}} </span> <i class="fas fa-long-arrow-alt-right fs22 align-middle invitees-navbar-right-arrow"></i></a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-12 col-md-8 offset-md-1 col-lg-9 offset-lg-1 invitees-left-spacing">
            <div class="row invitees_active_tab invitees-add-div">

                {{-- <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation">
                    <div>
                        <img src="images/Hari_Raini.jpg" class="img-fluid rounded-circle invitees_img" alt="" />
                    </div>
                    <div class="invites-mem-name-n-designation pt-2">
                        <div class="text-uppercase">MR. HARI RAINI</div>
                        <div class="text-capitalize">
                            <a href="#" class="text-white text-decoration-none" title="Member Of Rajya Sabha">Member of XYZ </a>
                        </div>
                    </div>
                </div>
 --}}



            </div>
        </div>
    </div>
</section>

<!-- End of Convention Invitees -->

<!-- Our Donors -->

<div class="container-fluid">
    <div class="row">
        <div class="col-12 pb50">
            <div class="main-heading">
                <div>
                    Our Donors
                </div>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid donors-bg">
    <section class="container py-3">
        <div class="row">
            <div class="col-12">
                <div>
                    <ul class="list-unstyled text-center horizontal-scroll">
                        @foreach($donortypes as $donortype)
                        <li class="donors-nav-item d-inline-flex">
                            <a class="donors-nav-link text-dark fs18 pt10 pb10 text-decoration-none  {{$loop->first ? 'first-date': '' }}" data-id="{{$donortype->id}}" data-url="{{env('APP_URL')}}/api/get-donor-with-typeid/{{$donortype->id}}">{{$donortype->name}}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="row donors_active_tab donors-add-div">



        </div>


    </section>
</section>

<!-- End of Our Donors -->

<!-- Events & Schedule -->

<section class="container-fluid my-5">
    <div class="row">
        <div class="col-12 pb-4">
            <div class="main-heading">
                <div>
                    Events & Schedule
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">

            @foreach($events as $event)
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                <div class="event-details ed-card ed-card1">
                    <div>
                        <img src="{{asset(config('conventions.event_display').$event->image_url)}}" class="img-fluid w-100 border-radius-top-right-15" alt="" />
                    </div>
                    <div class="p-3 event-date1">
                        <div class="fs18 text-white text-left py-2">
                            <span>{{ $event->from_date->format('D') }},</span>
                            <span> {{ \Carbon\Carbon::parse($event->from_date)->isoFormat('MMM Do YYYY')}}</span>
                        </div>
                        <div class="text-white text-center event-schedule">
                            <span class="text-white">DATE:</span> {{ \Carbon\Carbon::parse($event->from_date)->isoFormat('MMM Do YYYY')}} <br />
                            {{$event->event_name}}
                            <a href="{{url('event-schedule',$event->id)}}" class="text-white">more... </a>
                        </div>
                    </div>
                    <div class="row event-time-n-kn px15">
                        <div class="col-6 px-0">
                            <div class="event-time-block">
                                <div class="event-time">
                                    <span><i class="far fa-clock pr-2"></i></span>
                                    <span>8:00 AM to 4:00 PM</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 px-0">
                            <div class="event-km">
                                <a href="{{url('event-schedule',$event->id)}}" class="text-decoration-none text-white">Know More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

<!-- End of Events & Schedule -->

<!-- Youtube Vedios -->

<section class="home-vedio-bg py70">
    <div class="container">
        <div class="row">
            @foreach($videos as $video)
            <div class="col-12 col-md-4 col-lg-4 my-3">
                <div class="position-relative inner-shadow">
                    <a href="https://www.youtube.com/embed/{{$video->youtube_video_id}}" data-fancybox-group="" class="various fancybox.iframe d-block">
                        <span class="video-icon-hover">&nbsp;</span>
                        <img src="https://img.youtube.com/vi/{{$video->youtube_video_id}}/3.jpg" alt="NRIVA 6th Global Convention Logo Launch" title="NRIVA 6th Global Convention Logo Launch" class="p5 img-fluid w-100" width="415" height="249" />
                    </a>
                </div>
                <div class="text-center fs16 pt10 tabhorizontal-font14 tablet-l-h22">{{$video->name}}</div>
            </div>
            @endforeach

            <!--      <div class="col-12 col-md-4 col-lg-4 my-3">
                <div class="position-relative inner-shadow">
                    <a href="https://www.youtube.com/embed/gGrfODuTcQc" data-fancybox-group="" class="various fancybox.iframe d-block">
                        <span class="video-icon-hover">&nbsp;</span>
                        <img src="images/youtube-image2.jpg" alt="NRIVA 6th Global Convention Logo Launch" title="NRIVA 6th Global Convention Logo Launch" class="p5 img-fluid w-100" width="415" height="249" />
                    </a>
                </div>
                <div class="text-center fs16 pt10 tabhorizontal-font14 tablet-l-h22">NRIVA 6th Global Convention Invitation from President and Chairman</div>
            </div>
            <div class="col-12 col-md-4 col-lg-4 my-3">
                <div class="position-relative inner-shadow">
                    <a href="https://www.youtube.com/embed/DtoQ1sW9JY4" data-fancybox-group="" class="various fancybox.iframe d-block">
                        <span class="video-icon-hover">&nbsp;</span>
                        <img src="images/youtube-image3.jpg" alt="NRIVA 6th Global Convention Logo Launch" title="NRIVA 6th Global Convention Logo Launch" class="p5 img-fluid w-100" width="415" height="249" />
                    </a>
                </div>
                <div class="text-center fs16 pt10 tabhorizontal-font14 tablet-l-h22">NRIVA 6th Global Convention Promo</div>
            </div> -->
        </div>
        <div class="col-12 pt-3 px-0">
            <div class="text-right">
                <a href="{{url('show-all-videos')}}" class="text-decoration-none font-weight-bold fs14">VIEW ALL<i class="fas fa-angle-right pl-2"></i></a>
            </div>
        </div>
    </div>
</section>

<!-- End of Youtube Vedios -->

<!-- Donors Section -->

<section class="container-fluid bg-white py-5">
    <div class="container">
        <div class="row">
            <div class="counter-block my-2">
                <div class="counter-width counter-card-bg1">
                    <div class="my-auto">
                        <span class="fs18">323</span>
                        <span class="float-right"><img src="images/counting-icons/donors.png" width="30" alt="" /></span>
                    </div>
                    <div class="pt-3">
                        <h5 class="mb-0">Donors</h5>
                    </div>
                </div>
            </div>
            <div class="counter-block my-2">
                <div class="counter-width counter-card-bg2">
                    <div class="my-auto">
                        <span class="fs18">8228</span>
                        <span class="float-right"><img src="images/counting-icons/visitors.png" width="30" alt="" /></span>
                    </div>
                    <div class="pt-3">
                        <h5 class="mb-0">Site Visitors</h5>
                    </div>
                </div>
            </div>
            <div class="counter-block my-2">
                <div class="counter-width counter-card-bg3">
                    <div class="my-auto">
                        <span class="fs18">57759</span>
                        <span class="float-right"><img src="images/counting-icons/registrations.png" width="30" alt="" /></span>
                    </div>
                    <div class="pt-3">
                        <h5 class="mb-0">Registrations</h5>
                    </div>
                </div>
            </div>
            <div class="counter-block my-2">
                <div class="counter-width counter-card-bg4">
                    <div class="my-auto">
                        <span class="fs18">50</span>
                        <span class="float-right"><img src="images/counting-icons/invitees.png" width="30" alt="" /></span>
                    </div>
                    <div class="pt-3">
                        <h5 class="mb-0">Invitees</h5>
                    </div>
                </div>
            </div>
            <div class="counter-block my-2 counter-block-5">
                <div class="counter-width counter-card-bg5">
                    <div class="my-auto">
                        <span class="fs18">0</span>
                        <span class="float-right"><img src="images/counting-icons/performance.png" width="30" alt="" /></span>
                    </div>
                    <div class="pt-3">
                        <h5 class="mb-0">Performance</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


{{-- this part of is to clone not to show in page -- start--}}
<div class="donor-template" style="display:none">
    <div class="col-12 col-sm-6 col-md-4 col-lg-2 my-3 px-2 main">
        <div>
            <img src="" class="img-fluid w-100 border-radius-top-right-15 donor-image-height donor-image" alt="" />
        </div>
        <div class="home-donor-name py-2">
            <h6 class="text-uppercase px-2 text-white text-center mb-0 donor-name">Mr. Hari Raini</h6>
        </div>
    </div>
</div>

<div class='see-more-template' style="display:none">
    <div class="col-12 py-2 main">
        <div class="text-center">
            <a href="donors_more_details.php" class="btn px-3 bg-info text-white text-decoration-none border-radius-20 see-more-url">See More</a>
        </div>
    </div>
</div>

<div class="see-more-leadership-template" style="display:none">
    <div class="col-12 main">
        <div class="pb50 text-right">
            <a href="convention_leadership_committee.php" class="see-more-url text-decoration-none text-white font-weight-bold fs14">VIEW ALL<i class="fas fa-angle-right pl-2"></i></a>
        </div>
    </div>
</div>

<div class="leadership-template" style="display:none">
    <div class="col-12 col-sm-6 col-md-4 col-lg-3 py50 main">
        <div class="hexagon">
            <div>
                <img src="images/Hari_Raini.jpg" class="mx-auto d-block rounded-circle mt-40 leadership-image" width="100" alt="" />
            </div>
            <div class="text-center my-2">
                <a href="#" class="leader-name leadership-name">Mr. Hari Raini</a>
            </div>
            <div class="text-center mb10 text-white leadership-designation">President</div>
        </div>
    </div>
</div>

<div class="invitees-template" style="display:none">
    <div class="col-6 col-sm-6 col-md-6 col-lg-3 my-3 home-inviter-name-n-designation main">
        <div>
            <img src="images/Hari_Raini.jpg" class="img-fluid rounded-circle invitees_img invitees-image" alt="" />
        </div>
        <div class="invites-mem-name-n-designation pt-2">
            <div class="text-uppercase invitees-name">MR. HARI RAINI</div>
            <div class="text-capitalize">
                <a href="#" class="text-white text-decoration-none invitees-designation">Member of XYZ </a>
            </div>
        </div>
    </div>

</div>

{{-- this part of is to clone not to show in page----  end --}}

@section('javascript')

<script>
    $(".leadership-nav-link").click(function() {
        //active_tab
        ajaxCall($(this).data('url'), 'get', null, showDataInLeadership)
        $(".leadership_active_tab_btn").removeClass("leadership-nav-link");
        $(".leadership_active_tab_btn").removeClass("leadership_active_tab_btn");
        $(this).addClass("leadership-nav-link");
        $(this).addClass("leadership_active_tab_btn");
    });

    $(".invitees-nav-link").click(function() {
        //active_tab
        ajaxCall($(this).data('url'), 'get', null, showDataInInvitees)
        $(".invitees_active_tab_btn").removeClass("invitees-nav-link");
        $(".invitees_active_tab_btn").removeClass("invitees_active_tab_btn");
        $(this).addClass("invitees-nav-link");
        $(this).addClass("invitees_active_tab_btn");

    });

    $(".donors-nav-link").click(function() {
        ajaxCall($(this).data('url'), 'get', null, showDataInOurDonors)
        //active_tab
        $(".donors_active_tab_btn").removeClass("donors-nav-link");
        $(".donors_active_tab_btn").removeClass("donors_active_tab_btn");
        $(this).addClass("donors-nav-link");
        $(this).addClass("donors_active_tab_btn");
    });

    $(document).ready(function() {
        $('.first-date').addClass("donors_active_tab_btn");
        $('.first-leadership-type').addClass("leadership_active_tab_btn");
        ajaxCall($('.first-leadership-type').data('url'), 'get', null, showDataInLeadership)
        ajaxCall($('.first-date').data('url'), 'get', null, showDataInOurDonors)
        ajaxCall($('.invitees_all_data').data('url'), 'get', null, showDataInInvitees)
    })

    function showDataInOurDonors(data) {
        $('.donors-add-div').empty()
        if (data.length == 0) {
            $('.donors-add-div').append('<center> <p>No Donors</p> </center>');
        } else {
            for (let i = 0; i < data.length; i++) {
                $(".donor-template").find('.main').first().clone()
                    .find(".donor-image").attr('src', "{{asset(config('conventions.member_display'))}}/" + data[i].member.image_url).end()
                    .find(".donor-name").text(data[i].member.name).end()
                    .appendTo($('.donors-add-div'));
            }
            $(".see-more-template").find('.main').first().clone()
                .find(".see-more-url").attr('href', "{{url('more-donor-details')}}/" + data[0].sub_type_id).end()
                .appendTo($('.donors-add-div'));
        }
    }

    function showDataInInvitees(data) {
        $('.invitees-add-div').empty()
        if (data.length == 0) {
            $('invitees-add-div').append('<center> <p>No Data</p> </center>');
        } else {
            for (let i = 0; i < data.length; i++) {
                $(".invitees-template").find('.main').first().clone()
                    .find(".invitees-image").attr('src', "{{asset(config('conventions.member_display'))}}/" + data[i].member.image_url).end()
                    .find(".invitees-name").text(data[i].member.name).end()
                    .find(".invitees-designation").text(data[i].designation.name).end()
                    .appendTo($('.invitees-add-div'));
            }

        }
    }

    function showDataInLeadership(data) {
        $('.leadership-add-div').empty()
        if (data.length == 0) {
            $('leadership-add-div').append('<center> <p>No Data</p> </center>');
        } else {
            for (let i = 0; i < data.length; i++) {
                $(".leadership-template").find('.main').first().clone()
                    .find(".leadership-image").attr('src', "{{asset(config('conventions.member_display'))}}/" + data[i].member.image_url).end()
                    .find(".leadership-name").text(data[i].member.name).end()
                    .find(".leadership-designation").text(data[i].designation.name).end()
                    .appendTo($('.leadership-add-div'));
            }
            $(".see-more-leadership-template").find('.main').first().clone()
                .find(".see-more-url").attr('href', "{{url('more-leadership-details')}}/" + data[0].sub_type_id).end()
                .appendTo($('.leadership-add-div'));
        }
    }
</script>

@endsection


@endsection
