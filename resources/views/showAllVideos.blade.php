@extends('layouts.user.base')
@section('content')


<section class="container-fluid my-5">
    <div class="container shadow-small px-sm-20 py-4 p-md-4 p40">
        <div class="row">
            @foreach($videos as $video)
            <div class="col-12 col-md-4 col-lg-4 my-3">
                <div class="position-relative inner-shadow">
                    <a href="https://www.youtube.com/embed/{{$video->youtube_video_id}}" data-fancybox-group="" class="various fancybox.iframe d-block">
                        <span class="video-icon-hover">&nbsp;</span>
                        <img src="https://img.youtube.com/vi/{{$video->youtube_video_id}}/3.jpg" alt="{{$video->name}}" title="{{$video->name}}" class="p5 img-fluid w-100" width="415" height="249" />
                    </a>
                </div>
                <div class="text-center fs16 pt10 tabhorizontal-font14 tablet-l-h22">{{$video->name}}</div>
            </div>
            @endforeach

        </div>
    </div>
</section>

@section('javascript')

@endsection


@endsection