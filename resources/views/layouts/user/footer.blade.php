<!-- Footer -->

    <footer class="py-4 mt-4">
        <div class="container">
            <div class="row">
                <div class="col-6 col-sm-6 col-md-3 col-lg-3 py-3 my-auto">
                    <div>
                        <img src="images/logo.png" class="logo img-fluid" />
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-3 col-lg-3 py-3">
                    <ul class="list-unstyled mb-0">
                        <li>
                            <a href="page-under-constrution.php"><span>Home</span></a>
                        </li>
                        <li>
                            <a href="page-under-constrution.php"><span>About</span></a>
                        </li>
                        <li>
                            <a href="page-under-constrution.php"><span>Organisation</span></a>
                        </li>
                        <li>
                            <a href="page-under-constrution.php"><span>membership</span></a>
                        </li>
                    </ul>
                </div>
                <div class="col-5 col-sm-6 col-md-3 col-lg-3 py-3">
                    <ul class="list-unstyled mb-0">
                        <li>
                            <a href="page-under-constrution.php"><span>Events</span></a>
                        </li>
                        <li>
                            <a href="page-under-constrution.php"><span>Media</span></a>
                        </li>
                        <li>
                            <a href="page-under-constrution.php"><span>Contact</span></a>
                        </li>
                    </ul>
                </div>
                <div class="col-7 col-sm-6 col-md-3 col-lg-3 py-3">
                    <h6 class="text-white font-weight-bold">Email Newsletter</h6>
                    <div class="form-group pt-3">
                        <div class="inputWithIcon">
                            <input type="text" name="" id="" class="form-control footer-form-control" placeholder="Enter Your Email" aria-describedby="helpId">
                            <i class="fas fa-long-arrow-alt-right"></i>
                            <!-- <i class="fas fa-eye"></i> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="container-fluid copy-right-bg">
        <div class="container">
            <div class="row">
                <div class="col-12 py-2">
                    <div class="text-center text-white font-weight-bold">
                        @ Copyrights 2022 Telangana Telugu Association All rights reserved.
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- End of Footer -->

        <!-- All scripts -->
        <!-- jQuery library -->

        <script src="{{asset('js/jquery.js')}}"></script>

        <!-- Bootstrap JavaScript -->

        <script src="{{asset('js/bootstrapjs.js')}}"></script>

        <!--Popups js-->

        <script src="{{asset('js/popperjs.js')}}"></script>

        <!--Datatables js-->

        <script src="{{asset('js/datatables.js')}}"></script>

        <!-- Extra js -->

        <script src="{{asset('js/extrajquery.js')}}"></script>

       <!--  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

        <script>
            AOS.init({
                duration: 2000,
            });
        </script> -->

        <script>
            function openNav() {
                document.getElementById("mySidenav").style.width = "250px";
            }

            function closeNav() {
                document.getElementById("mySidenav").style.width = "0";
            }
            $(".convention-team-nav-link").click(function () {
        //active_tab
        $(".convention-team_active_tab_btn").removeClass("convention-team-nav-link");
        $(".convention-team_active_tab_btn").removeClass("convention-team_active_tab_btn");
        $(this).addClass("convention-team-nav-link");
        $(this).addClass("convention-team_active_tab_btn");

        $(".convention-team_active_tab").hide();
        $(".convention-team_active_tab").removeClass("convention-team_active_tab");
        $($(this).attr("target-id")).addClass("convention-team_active_tab");
        $($(this).attr("target-id")).show();
    });
        </script>
    </body>
</html>