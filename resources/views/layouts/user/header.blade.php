<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Home Page</title>

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <!-- Bootstrap CSS -->

        <link href="{{asset('css/bootstrapcss.css')}}" rel="stylesheet" />

        <!-- Fontawesome icons -->

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.2/css/all.css" />

        <!-- Animations -->

        <link href="{{asset('css/Animations.css')}}" rel="stylesheet" />

        <!-- Font family -->

        <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet" />

        <!--Datatables CSS-->

        <link href="{{asset('css/datatables.css')}}" rel="stylesheet" />

        <!-- AOS CSS -->

        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

        <!--Extra CSS-->

        <link href="{{asset('css/custom.css')}}" rel="stylesheet" />

        <link href="{{asset('css/tta.css')}}" rel="stylesheet" />

        <link href="{{asset('css/sidebar.css')}}" rel="stylesheet" />
    </head>

    <!-- <style type="text/css">

        ::placeholder {
            color: #fff !important;
        }

        :-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: #fff !important;
        }

        ::-ms-input-placeholder { /* Microsoft Edge */
            color: #fff !important;
        }
        
    </style> -->

    <body>
        <!--Goes Up-->

        <div class="topa">
            <i class="fas fa-arrow-up"></i>
        </div>

        <!-- Header -->

        <div class="container-fluid top-layer-bg py-1">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-5 col-lg-4">
                        <div class="text-center text-lg-left pl-lg-5">
                            <a href="#" class="text-white text-decoration-none fs15">Follow Us on: <i class="fab fa-facebook-f px-2"></i><i class="fab fa-twitter px-2"></i><i class="fab fa-youtube px-2"></i></a>
                        </div>
                    </div>
                    <div class="col-12 col-md-7 offset-lg-1 col-lg-7">
                        <div class="text-center text-lg-right pr-lg-5">
                            <a href="#" class="text-white text-decoration-none helpline">Helpline : 1-866-TTA-SEVA (1-866-882-7382)</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 px-0">
                    <div>
                        <img src="{{asset('images/thoranam.png')}}" class="img-fluid" alt="Thoranam">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid d-block d-lg-none">
            <div class="row">
                <div class="col-12">
                    <div class="text-danger font-weight-bold">
                        <div class="web-name website-name text-center py-2">TELANGANA AMERICAN TELUGU ASSOCIATION</div>
                        <div class="web-name website-name text-center d-block pb-2">తెలుగు కళల తోట | తెలంగాణ సేవల కోట </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid logos_bg py-2 d-none d-lg-block">
            <div class="container mx-auto">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="text-center">
                            <a href="index.php" class="text-decoration-none">
                                <div class="d-flex">
                                    <div class="my-auto pl45">
                                        @if($data['left_logo'] !=null )
                                         <img src="{{asset(config('conventions.logo_display').$data['left_logo']->image_url)}}" width="60" alt="" border="0" class="img-fluid" />
                                        <!-- <img src="{{asset('images/tta_logo.png')}}" class="logo img-fluid" /> -->
                                        @endif
                                    </div>
                                    <div class="my-auto pl80">
                                        <span class="pl20 my-auto text-danger">
                                            <strong class="web-name website-name">TELANGANA AMERICAN TELUGU ASSOCIATION</strong>
                                            <strong class="fs22 d-block website-name">తెలుగు కళల తోట | తెలంగాణ సేవల కోట </strong>
                                        </span>
                                    </div>
                                    <div class="my-auto pl95">
                                        @if($data['right_logo'] !=null )
                                        <img src="{{asset(config('conventions.logo_display').$data['right_logo']->image_url)}}" alt="" border="0" width="60" class="img-fluid" />
                                        @endif
                                        <!-- <img src="{{asset('images/tta_convention.png')}}" class="logo img-fluid" /> -->
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid bottom-layer-bg d-none d-lg-block">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <nav class="navbar navbar-expand-lg navbar-light py-0 justify-content-center d-none d-lg-block">

                            <!-- Navbar links -->

                            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                                <ul class="navbar-nav">
                                    @foreach($data['menu'] as $menu)
                                    <li class="nav-item {{$menu->slug==null? 'dropdown': '' }} ">
                                        <a class="nav-link" href="{{env('APP_URL')}}/{{$menu->slug==null ? '#': $menu->slug }}" id="navbardrop" {{$menu->slug==null? 'data-toggle="dropdown"': '' }}>{{$menu->name}}</a>
                                        <div class="dropdown-menu submenu s-row">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul class="list-unstyled">
                                                        @foreach($menu->submenus as $submenu)
                                                        <li>
                                                            <a href="{{env('APP_URL')}}/{{$submenu->slug }}">{{$submenu->name}}</a>
                                                        </li>
                                                         @endforeach
                                                        <!-- <li>
                                                            <a href="venue_details.php">Venue</a>
                                                        </li>
                                                        <li>
                                                            <a href="attractions_details.php">Attractions</a>
                                                        </li> -->
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach
                                    <!-- <li class="nav-item dropdown">
                                        <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                            Registration
                                        </a>
                                        <div class="dropdown-menu submenu l-menu">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="#" class="">Awards</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">CME</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Exhibits</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Shathamanam Bhavathi</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Youth Activities</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="#" class="">Business Conference</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Convention</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Matrimony</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">TTA Got Talent</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Lakshmi Narasimha Swami Kalyanam</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="#" class="">Business Plan Competition</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Cultural</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Miss. TTA &amp; Mrs. TTA</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Women's Conference</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Souvenir Registrations</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                            Team
                                        </a>
                                        <div class="dropdown-menu submenu s-row">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="advisory_council.php">Advisory Council</a>
                                                        </li>
                                                        <li>
                                                            <a href="founding_members.php">Founding Members</a>
                                                        </li>
                                                        <li>
                                                            <a href="executive_committee.php">Executive Committee</a>
                                                        </li>
                                                        <li>
                                                            <a href="board_of_directors.php">Board Of Directors</a>
                                                        </li>
                                                        <li>
                                                            <a href="standing_committe_members.php">Standing Committe Members</a>
                                                        </li>
                                                        <li>
                                                            <a href="events_n_schedule.php">Events & Schedule</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li> 
                                    <li class="nav-item dropdown">
                                        <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                            Speakers
                                        </a>
                                        <div class="dropdown-menu submenu s-row">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="#">Business Speakers</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Youth Conference Speakers</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Stock &amp; Investments Speakers</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Immigration Forum Speakers</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Women's Speakers</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Real Estate</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Spiritual Speaker</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li> 
                                    <li class="nav-item dropdown">
                                        <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                            Committees
                                        </a>
                                        <div class="dropdown-menu submenu l-menu">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="banquet_night.php" class="">Banquette</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Budget & Finance</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Business</a>
                                                        </li>
                                                        <li>
                                                            <a href="cme_details.php" class="">CME</a>
                                                        </li>
                                                        <li>
                                                            <a href="cultural_registrations.php" class="">Cultural</a>
                                                        </li>                      <li>
                                                            <a href="#" class="">Decoration</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Food</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Hospitality</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Literacy</a>
                                                        </li>
                                                        <li>
                                                            <a href="matrimony_registration.php" class="">Matrimonial</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="#" class="">Media & Communication</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Overseas Coordination</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Political Forum</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Programs and Events</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Reception</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Registration</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Safety & Security</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Short Film</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Social Media</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Souvenir</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="#" class="">Spiritual</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Stage & AV</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">TTA Star</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Transportation</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Vendors & Exhibits</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Web Committee</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Women Committee</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="">Youth Forum</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li> 
                                    <li class="nav-item">
                                        <a class="nav-link" href="exhibits_reservation.php">Exhibits</a>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link" href="page-under-constrution.php">
                                            Schedule
                                        </a>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                            Donors
                                        </a>
                                        <div class="dropdown-menu submenu s-row">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="page-under-constrution.php">Sapphire</a> 
                                                        </li>
                                                        <li>
                                                            <a href="page-under-constrution.php">Diamond</a>
                                                        </li>
                                                        <li>
                                                            <a href="page-under-constrution.php">Platinum</a>
                                                        </li>
                                                        <li>
                                                            <a href="page-under-constrution.php">Gold</a>
                                                        </li>
                                                        <li>
                                                            <a href="page-under-constrution.php">Silver</a>
                                                        </li>
                                                        <li>
                                                            <a href="page-under-constrution.php">Bronze</a>
                                                        </li>
                                                        <li>
                                                            <a href="page-under-constrution.php">Patron</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li> 
                                    <li class="nav-item dropdown">
                                        <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                            Logistics
                                        </a>
                                        <div class="dropdown-menu submenu s-row">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="hotels_details.php">Hotels</a>
                                                        </li>
                                                        <li>
                                                            <a href="venue_details.php">Venue</a>
                                                        </li>
                                                        <li>
                                                            <a href="attractions_details.php">Attractions</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li> -->
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Contact&nbsp;Us</a>
                                    </li>
                                    <li class="nav-item active">
                                        <a class="nav-link lh25 text-white" href="signin.php">
                                            Login
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <nav class="navbar-dark d-block d-lg-none py-2">
            <div class="navbar">
                <!-- Brand -->

                <a class="navbar-brand py-0" href="index.php">
                     @if($data['right_logo'] !=null )
                    <img src="{{asset(config('conventions.logo_display').$data['right_logo']->image_url)}}" alt="" border="0" width="60" class="img-fluid" />
                    @endif
                </a>

                <div class="nav-item">
                    <a class="nav-link px-1" href="#">
                        <span class="text-dark fs40 font-weight-bold" onclick="openNav()"><i class='fas fs30'>&#xf039;</i></span>
                    </a>
                </div>
            </div>
        </nav>

        <div id="mySidenav" class="sidenav d-block d-lg-none">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <ul class="navbar-nav">
                <li class="sidemenu-nav-item sidemenu-active">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Home</span>
                    </a>
                </li>
                <li class="sidemenu-nav-item sidemenu-dropdown">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Registration</span><i class="fas fa-angle-right float-right pt4"></i>
                    </a>
                    <ul class="submenu d-none">
                        <li><a href="#">Awards</a></li>
                        <li><a href="#">Business Conference</a></li>
                        <li><a href="#">Business Plan Competition</a></li>
                        <li><a href="#">CME</a></li>
                        <li><a href="#">Convention</a></li>
                        <li><a href="#">Cultural</a></li>
                        <li><a href="#"> Exhibits</a></li>              
                        <li><a href="#">Matrimony</a></li>
                        <li><a href="#">Miss. TTA &amp; Mrs. TTA</a></li>
                        <li><a href="#">Shathamanam Bhavathi</a></li>
                        <li><a href="#">Vasavites Got Talent</a></li>
                        <li><a href="#">Women's Conference</a></li>
                        <li><a href="#">Youth Activities</a></li>
                        <li><a href="#">Lakshmi Narasimha Swami Kalyanam</a></li>
                        <li><a href="#">Souvenir Registrations</a></li>
                    </ul>
                </li>
                <li class="sidemenu-nav-item sidemenu-dropdown">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Team</span><i class="fas fa-angle-right float-right pt4"></i>
                    </a>
                </li>
                <li class="sidemenu-nav-item sidemenu-dropdown">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Speakers</span><i class="fas fa-angle-right float-right pt4"></i>
                    </a>
                </li>
                <li class="sidemenu-nav-item">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Exhibits</span>
                    </a>
                </li>
                <li class="sidemenu-nav-item">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Schedule</span>
                    </a>
                </li>
                <li class="sidemenu-nav-item sidemenu-dropdown">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Donors</span><i class="fas fa-angle-right float-right pt4"></i>
                    </a>
                </li>
                <li class="sidemenu-nav-item sidemenu-dropdown">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Logistics</span><i class="fas fa-angle-right float-right pt4"></i>
                    </a>
                </li>
            </ul>
        </div>

        <!-- End Of Header -->

        