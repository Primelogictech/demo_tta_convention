<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <title>Home Page</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('admin_asserts/img/favicon.png')}}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('admin_asserts/css/bootstrap.min.css')}}">

    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{asset('admin_asserts/css/font-awesome.min.css')}}">

    <!-- Feathericon CSS -->
    <link rel="stylesheet" href="{{asset('admin_asserts/css/feathericon.min.css')}}">

    <link rel="stylesheet" href="{{asset('admin_asserts/plugins/morris/morris.css')}}">

    <!-- Main CSS -->
    <link rel="stylesheet" href="{{asset('admin_asserts/css/style.css')}}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('admin_asserts/css/custom.css')}}">

    <link rel="stylesheet" href="{{asset('admin_asserts/css/custom1.css')}}">

    <!-- Font Family -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

</head>

<body>


    @include('layouts.admin.header')
    @include('layouts.admin.sidebar')

    @if ($message = Session::get('success'))
    @dd($message)
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    @endif


    @if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    @endif


    @if ($message = Session::get('warning'))
    <div class="alert alert-warning alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    @endif


    @if ($message = Session::get('info'))
    <div class="alert alert-info alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    @endif


    @if(Session::has('message-suc'))
    <script type="text/javascript">
        swal({
            title: "success!",
            text: "{{ Session::get('message-suc') }}!",
            icon: "success",
        });
    </script>
    @endif

    <!-- Page Content -->
    <div class="page-wrapper">
        <div class="content container-fluid">
            @if ($errors->any())
            @foreach ($errors->all() as $error)
            <p class="alert alert-danger">{{ $error }}</p>
            @endforeach
            @endif

            @yield('content')
        </div>
    </div>
</body>

@include('layouts.admin.footer')
<script src="{{ asset('js/common.js') }}"></script>
@yield('javascript')


<script type="text/javascript">
    $(window).on('load', function() {
        var url = window.location.href;
        url = url.split("?")[0]
        //$("body").find('a[class="nav-link header-nav-link"]').removeClass("active");
        // $("body").find('a[href="'+url+'"]').addClass("show");
        //  $("body").find('a[href="' + url + '"] ul ').hide();
        $("body").find('a[href="' + url + '"] li').addClass('active');
        $("body").find('a[href="' + url + '"]').parent().addClass('active')
        $("body").find('a[href="' + url + '"]').parent().parent().show()
        $("body").find('a[href="' + url + '"]').parent().parent().siblings().addClass('subdrop')
    });
</script>

</html>
