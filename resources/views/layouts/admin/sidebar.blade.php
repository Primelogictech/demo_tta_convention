	<!-- Sidebar -->
	<div class="sidebar" id="sidebar">
		<div class="sidebar-inner slimscroll">
			<div id="sidebar-menu" class="sidebar-menu">
				<ul>
					<li class="active1">
						<a href="{{url('admin')}}"><i class="fe fe-home"></i> <span>Dashboard</span></a>
					</li>
					<li class="active1">
						<a href="{{url('registrations')}}"><i class="fe fe-home"></i> <span>Registrations</span></a>
					</li>
					<li class="active1">
						<a href="{{url('exhibit_registrations')}}"><i class="fe fe-home"></i> <span>Exhibit registrations</span></a>
					</li>
					<li class="submenu">
						<a href="#"><i class="fe fe-document"></i> <span> Home Page </span> <span class="menu-arrow"></span></a>
						<ul style="display: none;">
							<li><a href="{{route('banner.index')}}">Upload Banners</a></li>
							<li><a href="{{route('venue.index')}}">Change Venue Date & Time</a></li>
							<li><a href="{{route('message.index')}}">Add Messages</a></li>

						<!-- 	<li><a href="leadership.php">Pillars Of Our Success Leadership</a></li> -->
							<li><a href="{{route('program.index')}}">Programs</a></li>
							<!-- <li><a href="invitees.php">Invitees</a></li> -->
							<li><a href="{{route('member.index')}}">Members</a></li>
						<!-- 	<li><a href="{{route('donor.index')}}">Donors</a></li> -->
							<li><a href="{{route('event.index')}}">Events & Schedule</a></li>
							<li><a href="{{route('schedule.index')}}">Schedule</a></li>
							<li><a href="{{route('video.index')}}">Youtube Videos</a></li>
						</ul>
					</li>
					<li class="submenu">
						<a href="#"><i class="fe fe-document"></i> <span> Content Mangement </span> <span class="menu-arrow"></span></a>
						<ul style="display: none;">
							<li><a href="{{route('menu.index')}}">Menu Mangement</a></li>
							<li><a href="{{route('submenu.index')}}">Menu Items Mangement</a></li>
							<li><a href="{{route('page.index')}}">Manage Pages</a></li>
						</ul>
					</li>
					<li class="submenu">
						<a href="#"><i class="fe fe-document subdrop"></i> <span> Master </span> <span class="menu-arrow"></span></a>
						<ul>
							 <li><a href="{{route('sponsor-category-type.index') }}">Sponsor Category Type</a></li>
	                        <li><a href="{{route('sponsor-category.index')}}">Manage Sponsor Category</a></li>
							<li><a href="{{route('right-logo.index') }}">Upload Right Side Logo</a></li>
							<li><a href="{{route('left-logo.index') }}">Upload Left Side Logo</a></li>
							<!-- <li><a href="{{route('sponsor-category-type.index') }}">Sponsor Category Type</a></li> -->
							<li><a href="{{ route('leadership-type.index')}}">Convention Teams</a></li>
							<li><a href="{{ route('leadership-type.index')}}">Convention Committees</a></li>
							<li><a href="{{ route('leadership-type.index')}}">Media Partners</a></li>
							<li><a href="{{ route('invitee.index')}}">Invitees Type</a></li>
							<li><a href="{{ route('donortype.index')}}">Donor Type</a></li>
							<li><a href="{{route('designation.index')}}">Manage Designation</a></li>
							<li><a href="{{route('benefittype.index')}}">Benifits Type</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /Sidebar -->