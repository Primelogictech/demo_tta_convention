@extends('layouts.user.base')
@section('content')



<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/fullcalendar@3.10.2/dist/fullcalendar.min.css'>
<link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/cupertino/jquery-ui.css'>
<link rel="stylesheet" href="css/style.css">

<style type="text/css">
    body {
        font-family: 'Kanit', sans-serif;
    }

    span.fc-title {
        white-space: normal;
    }

    .fc-view-container {
        overflow-x: scroll;
    }

    /*table tr td, table tr th, td.fc-day-top.fc-tue.fc-future{
            width: 150px !important;
        }*/
</style>

<section class="container-fluid py-5">
    <div class="row">
        <div class="col-12">
            <div class="main-heading">
                <div>
                    Convention Events
                </div>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid mt-2 mt-lg-4 mb-5">
    <div class="shadow-small px-sm-20 py-4 px-md-3 py-md-4 p20">
        <div class="row px-2 px-md-3">
            <div class="col-12 col-md-12 col-lg-4 shadow-xs py-3">
                <div class="row">
                    <div class="col-12 my-auto">
                        <div class="fs20 font-weight-bold">Events Schedule</div>
                    </div>
                    <!-- <div class="col-6 my-auto">
                            <div class=" text-right">
                                <button class="btn btn-info"><i class="fas fa-plus fs14 pr-2"></i> New</button>
                            </div>
                        </div>   -->
                </div>
                <div class="col-12 shadow-xs mt-3 calender-events-schedule">
                    <div class="row event-add-div">
                    </div>
                    <div class="event-template" style="display:none">
                        <div class="col-12 px-0 border-bottom bg-light main">
                            <div class="px-2 pt30 pb30">
                                <div class="border-left-2-violet  pl-2">
                                    <div>
                                        <span class="text-violet font-weight-bold fs16 event-name">name</span>
                                    </div>
                                    <div class="description"></div>
                                    <div class="my-2">
                                        <!-- <span class="bg-grey fs13 px-2 py3 border-radius-3 mr-2">
                                            <a href="#" class="text-dark text-decoration-none"><i class="far fa-user"></i> &nbsp;|&nbsp;Tim</a>
                                        </span> -->
                                        <span class="bg-grey fs13 px-2 py3 border-radius-3 mr-2">
                                            <a href="#" class="text-dark text-decoration-none event-date"><i class="far fa-calendar-alt"></i> &nbsp;|&nbsp; date</a>
                                        </span>
                                    </div>
                                    <a href="" class="event-url">View More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-8 px-0 pl-md-3 pr-md-0">
                <div id='calendar' class="shadow-xs p-3 mt-0"></div>
            </div>
        </div>
    </div>
</section>

@section('javascript')

<script src='https://cdn.jsdelivr.net/npm/moment@2.24.0/min/moment.min.js'></script>
<script src='https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.min.js'></script>
<script src='https://cdn.jsdelivr.net/npm/fullcalendar@3.10.2/dist/fullcalendar.min.js'></script>
{{-- <script src="js/script.js"></script>
 --}}
<script>
    var calendar = $('#calendar').fullCalendar({
        defaultView: 'agenda',
        viewRender: function(view, element) {
            start = $('#calendar').fullCalendar("getView").start.format()
            end = $('#calendar').fullCalendar("getView").end.format()
            showEventsOnRightSide(start, end)
        },
        visibleRange: function(currentDate) {
            return {
                start: '{{ $venue->Event_date_time }}',
                end: "{{$venue->end_date->addDays(1) }}" // exclusive end, so 3
            };
        },
        themeSystem: 'jquery-ui',
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'agendaWeek,agendaDay,listMonth'
        },
        weekNumbers: true,
        eventLimit: true, // allow "more" link when too many events
        //events: 'https://fullcalendar.io/demo-events.json'
        events: '{{ url("api/get-events-in-date") }}'
    });

    function showEventsOnRightSide(start, end) {
        ajaxCall('{{url("api/get-events-in-leftside")}}?start=' + start + '&end=' + end, 'get', null, updateEventsView)
    }

    function updateEventsView(data) {
        $('.event-add-div').empty()
        data.forEach(element => {
            console.log(element)
            $(".event-template").find('.main').first().clone()
                .find(".event-name").text(element.event_name).end()
                .find(".event-url").attr('href', '{{ url("event-schedule") }}' +'/' +element.id).end()
                .find(".event-date").html('<i class="far fa-calendar-alt"></i> &nbsp;|&nbsp; ' + element.from_date + " To " + element.to_date).end()
                .find(".description").html(element.description).end()
                .appendTo($('.event-add-div'));
        });
    }
</script>



@endsection


@endsection
