<?php

namespace Tests\Feature\Http\Controllers\Admin;

use App\Models\Admin\LeadershipType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\Admin\LeadershipTypeController
 */
class LeadershipTypeControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_displays_view()
    {
        $leadershipTypes = LeadershipType::factory()->count(3)->create();

        $response = $this->get(route('leadership-type.index'));

        $response->assertOk();
        $response->assertViewIs('leadershipType.index');
        $response->assertViewHas('leadershipTypes');
    }


    /**
     * @test
     */
    public function create_displays_view()
    {
        $response = $this->get(route('leadership-type.create'));

        $response->assertOk();
        $response->assertViewIs('leadershipType.create');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\Admin\LeadershipTypeController::class,
            'store',
            \App\Http\Requests\LeadershipTypeStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $name = $this->faker->name;
        $status = $this->faker->boolean;

        $response = $this->post(route('leadership-type.store'), [
            'name' => $name,
            'status' => $status,
        ]);

        $leadershipTypes = LeadershipType::query()
            ->where('name', $name)
            ->where('status', $status)
            ->get();
        $this->assertCount(1, $leadershipTypes);
        $leadershipType = $leadershipTypes->first();

        $response->assertRedirect(route('leadershipType.index'));
        $response->assertSessionHas('leadershipType.id', $leadershipType->id);
    }


    /**
     * @test
     */
    public function show_displays_view()
    {
        $leadershipType = LeadershipType::factory()->create();

        $response = $this->get(route('leadership-type.show', $leadershipType));

        $response->assertOk();
        $response->assertViewIs('leadershipType.show');
        $response->assertViewHas('leadershipType');
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $leadershipType = LeadershipType::factory()->create();

        $response = $this->get(route('leadership-type.edit', $leadershipType));

        $response->assertOk();
        $response->assertViewIs('leadershipType.edit');
        $response->assertViewHas('leadershipType');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\Admin\LeadershipTypeController::class,
            'update',
            \App\Http\Requests\LeadershipTypeUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_redirects()
    {
        $leadershipType = LeadershipType::factory()->create();
        $name = $this->faker->name;
        $status = $this->faker->boolean;

        $response = $this->put(route('leadership-type.update', $leadershipType), [
            'name' => $name,
            'status' => $status,
        ]);

        $leadershipType->refresh();

        $response->assertRedirect(route('leadershipType.index'));
        $response->assertSessionHas('leadershipType.id', $leadershipType->id);

        $this->assertEquals($name, $leadershipType->name);
        $this->assertEquals($status, $leadershipType->status);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $leadershipType = LeadershipType::factory()->create();

        $response = $this->delete(route('leadership-type.destroy', $leadershipType));

        $response->assertRedirect(route('leadershipType.index'));

        $this->assertDeleted($leadershipType);
    }
}
