<?php
return [
    'banner_upload' =>'public/banner/',
    'banner_display' =>'public/storage/banner/',

    'message_upload' => 'public/message/',
    'message_display' => 'public/storage/message/',

    'logo_upload' => 'public/logo/',
    'logo_display' => 'public/storage/logo/',

    'event_upload' => 'public/event/',
    'event_display' => 'public/storage/event/',

    'program_upload' => 'public/program/',
    'program_display' => 'public/storage/program/',


    'donor_upload' => 'public/donor/',
    'donor_display' => 'public/storage/donor/',

    'member_upload' => 'public/member/',
    'member_display' => 'public/storage/member/',
    
];
