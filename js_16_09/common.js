function convertToSlug(Text) {
    return Text
        .toLowerCase()
        .replace(/ +/g, '-')
       .replace(/[^\w-]+/g, '')
}


$(document).ready(function () {
    $('[name=title], [name=name]').on('keyup', function () {
        var title = $(this).val();
        var slug = convertToSlug(title);
        $('[name=slug]').val(slug);

    });


    $('[name=title], [name=name]').on('blur', function () {
        var title = $(this).val();
        var slug = convertToSlug(title);
        $('[name=slug]').val(slug);

    });


    $('[name=slug]').on('keyup', function () {
        var title = $(this).val();
        var slug = convertToSlug(title);
        $('[name=slug]').val(slug);

    });

    $('[name=slug]').on('blur', function () {
        var title = $(this).val();
        var slug = convertToSlug(title);
        $('[name=slug]').val(slug);
    });
})


function IsCheckBoxChecked(thisVal) {
    if (thisVal.checked) {
           return true
        } else {
            return false
        }
}


function ajaxCall(url, method, data = null, callBack = null) {
    $.ajax({
        url: url,
        type: method,
        data: data,
        success: function (response) {
            if (callBack != null) {
                callBack(response);
            }
        }
    });
}



$('.status-btn').click(function() {
        if ($(this).text() == 'Active') {
        $(this).text('InActive')
            status = 0
        } else {
        $(this).text('Active')
            status = 1
        }
    data = {
        "_token": $('meta[name=csrf-token]').attr('content'),
    id: $(this).attr('data-id'),
    status: status
        }
        ajaxCall($(this).data('url'), 'put', data, afterStatusUpdate)

    function afterStatusUpdate(data) {

    }
    })

$('.delete-btn').click(function () {

    swal({
        title: "Are you sure ?",
        text: "Do you want to delete ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if(willDelete){
            data = {
                        "_token": $('meta[name=csrf-token]').attr('content'),
                        id: $(this).attr('data-id'),
                    }
                ajaxCall($(this).data('url'), 'delete', data, afterdelete)


            }
        });

})

function afterdelete(data) {
    location.reload();
    //  console.log(data)
}


$('.dropdown-element').change(function () {
    ajaxCall($(this).data('url') + '/' + $(this).val(), 'get', null , putDataToDropdown)
})


$('.dropdown-element-edit').change(function () {
    ajaxCall($(this).data('url') + '/' + $(this).val()+'/'+$(this).data('id'), 'get', null, putDataToDropdown)
})



function putDataToDropdown(data) {
    $('.dropdown-target').empty()
    $('.dropdown-target').append('<option selected disabled value="">Select</option>')
    for (let i = 0; i < data.length; i++) {
        $('.dropdown-target').append('<option  value="'+ data[i].id+'">'+data[i].name+'</option>')
    }
}
